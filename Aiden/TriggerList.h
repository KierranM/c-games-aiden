#pragma once

//Predeclaration of the AnimatedTile to prevent a compiler loop
ref class AnimatedTile;

/*
	Author: Kierran McPherson
	Date: 18/11/2013
	Purpose: This class defines a LinkedList of AnimatedTiles that act as triggers to Another AnimatedTile
	Known Bugs:
*/

ref class TriggerList
{
private:
	AnimatedTile^ head;
	AnimatedTile^ tail;
public:
	TriggerList(void);
	TriggerList(AnimatedTile^ first);

	int Count();
	void Add(AnimatedTile^ node);
	void Remove(AnimatedTile^ node);
	bool AllTriggered();

};
