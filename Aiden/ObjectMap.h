#pragma once
#include "AnimatedTile.h"

#define TILE_SIZE 64

using namespace System;
using namespace System::ComponentModel;
using namespace System::Collections;
using namespace System::Windows::Forms;
using namespace System::Data;
using namespace System::Drawing;
using namespace System::IO;

public enum EObjectType{
	Air,
	Switch,
	DoorTop,
	DoorMid,
	DoorBottom,
	ExitTopLeft,
	ExitTopRight,
	ExitMidLeft,
	ExitMidRight,
	ExitBottomLeft,
	ExitBottomRight
};

/*
	Author: Kierran McPherson
	Date: 18/11/2013
	Purpose: This class defines an ObjectMap, which is like a TileMap but instead of holding an array of ints
			 it holds an array of AnimatedTiles which are the objects in the game
	Known Bugs:
*/

ref class ObjectMap
{
private:
	int rows;
	int cols;
	int tileSize;
	array<AnimatedTile^, 2>^ tiles;
	
public:
	ObjectMap(int startRows, int startCols, int startTileSize);

	void LoadFromFile(String^ filepath);
	void LoadTriggersFromFile(String^ filepath);

	void TriggerTileAtPoint(Point p);
	void UpdateAll();

	//Gets and Sets
	AnimatedTile^ GetTile(int xIndex, int yIndex) {return tiles[yIndex, xIndex];}
	void SetTile(int xIndex, int yIndex, AnimatedTile^ tile) {tiles[yIndex,xIndex] = tile;}

	void SetTileTrigger(int xIndex, int yIndex, int triggerX, int triggerY);

	int GetRows()	{return tiles->GetLength(0);}
	int GetColumns()	{return tiles->GetLength(1);}
	int GetTileSize()	{return tileSize;}

	bool GetIsSolid(int xIndex,int yIndex) {return tiles[yIndex,xIndex]->GetIsSolid();}
	bool GetIsLethal(int xIndex, int yIndex)	{return tiles[yIndex,xIndex]->GetIsLethal();}
	bool GetIsAidenBarrier(int xIndex, int yIndex) {return tiles[yIndex,xIndex]->GetIsAidenBarrier();}
	bool GetIsVictoryTile(int xIndex, int yIndex)	{return tiles[yIndex, xIndex]->GetIsVictoryTile();}

	Bitmap^ GetMapCellBitmap(int xIndex, int yIndex)	{return tiles[yIndex,xIndex]->GetTileImage();}
};
