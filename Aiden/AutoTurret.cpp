#include "stdafx.h"
#include "AutoTurret.h"


AutoTurret::AutoTurret(
		array<String^, TWO>^ filenames, 
		array<int,TWO>^ frameNumbers, 
		array<Size>^ frameSize,
		Point startLocation,
		int startAction,
		int startXDeadZone,
		int startYDeadZone,
		Graphics^ startCanvas,
		Random^ startRandom,
		Rectangle startBounds,
		TileMap^ startMap,
		ObjectMap^ startObjects,
		ProjectileList^ startBullets
	)
	:FiniteStateCharacter(
		filenames,
		frameNumbers,
		frameSize,
		startLocation,
		startAction,
		startXDeadZone,
		startYDeadZone,
		startCanvas, 
		startRandom,
		startBounds,
		startMap,
		startObjects,
		false,
		0,
		0,
		0,
		0
	)
{
	aimingLocation = Point(0,0);
	triggers = gcnew TriggerList();
}//End constructor 12 params

void AutoTurret::UpdateState(AidenCharacter^ player, BitArray^ keys)
{
	//performs the FSM logic to update the state of the turret based on the events that have occured

	//Switch on the turrets current action
	switch ((AutoTurretStates::ECharacterState)currentAction)
	{
		case AutoTurretStates::ECharacterState::Monitoring:

			//Is the Possessed NPC in viewing distance
			if (PossessedNPCInLOS(player, AUTO_TURRET_VIEW_DISTANCE, false))
			{
				currentAction = AutoTurretStates::ECharacterState::Aiming;
				doneAiming = false;
				//Set the aiming point to be a random location near the possessed NPC's center
				Point center = player->GetCenter();
				aimingLocation.X = center.X + random->Next(-AIMING_INACCURACY,AIMING_INACCURACY);
				aimingLocation.Y = center.Y + random->Next(-AIMING_INACCURACY,AIMING_INACCURACY);
				System::Diagnostics::Debug::WriteLine("Auto Turret switch from Monitoring to Aiming");
			}

			//is the turret not alive (powered off)
			if (!isAlive || triggers->AllTriggered())
			{
				isAlive = false;
				currentAction = AutoTurretStates::ECharacterState::Disabled;
				System::Diagnostics::Debug::WriteLine("Auto Turret switch from Monitoring to Disabled");
			}
			break;//End Monitoring case

		case AutoTurretStates::ECharacterState::Aiming:

			//has the target moved out of range
			if (PossessedNPCInLOS(player, AUTO_TURRET_VIEW_DISTANCE, false) == false)
			{
				currentAction = AutoTurretStates::ECharacterState::Monitoring;
				System::Diagnostics::Debug::WriteLine("Auto Turret switch from Aiming to Monitoring");
			}

			//Has the turret finished aiming
			if (doneAiming)
			{
				currentAction = AutoTurretStates::ECharacterState::Firing;
				System::Diagnostics::Debug::WriteLine("Auto Turret switch from Aiming to Firing");
			}

			//is the turret not alive (powered off)
			if (!isAlive || triggers->AllTriggered())
			{
				isAlive = false;
				currentAction = AutoTurretStates::ECharacterState::Disabled;
				System::Diagnostics::Debug::WriteLine("Auto Turret switch from Aiming to Disabled");
			}
			break;//End Aiming case
		case AutoTurretStates::ECharacterState::Firing:

			//Has the npc died or become depossessed
			if (player->GetPossessing() == nullptr || player->GetPossessing()->GetIsAlive())
			{
				currentAction = AutoTurretStates::ECharacterState::Monitoring;
				System::Diagnostics::Debug::WriteLine("Auto Turret switch from Firing to Monitoring");
			}

			//Is the NPC no longer in the correct aiming zone
			if (player->GetPossessing() != nullptr && InAimingZone(player->GetPossessing()))
			{
				currentAction = AutoTurretStates::ECharacterState::Aiming;
				System::Diagnostics::Debug::WriteLine("Auto Turret switch from Firing to Aiming");
			}

			//is the turret not alive (powered off)
			if (!isAlive || triggers->AllTriggered())
			{
				isAlive = false;
				currentAction = AutoTurretStates::ECharacterState::Disabled; 
				System::Diagnostics::Debug::WriteLine("Auto Turret switch from Firing to Disabled");
			}
			break;//End Firing case

		case AutoTurretStates::ECharacterState::Disabled:
			if (isAlive)
			{
				currentAction = AutoTurretStates::ECharacterState::Monitoring;
				System::Diagnostics::Debug::WriteLine("Auto Turret switch from Disabled to Monitoring");
			}
			break;//End of Disabled case
	}//End of switch statement
}//End UpdateState(player,keys)

void AutoTurret::PerformActions(AidenCharacter^ player, BitArray^ keys)
{
	
	//Switch on the turrets current action
	switch ((AutoTurretStates::ECharacterState)currentAction)
	{
		case AutoTurretStates::ECharacterState::Monitoring:
			currentDirection = (eDirection)random->Next(0,TWO);
			break;
		case AutoTurretStates::ECharacterState::Aiming:
			//Is the player still possessing someone
			if (player->GetPossessing() != nullptr)
			{
				//Get the center of the possessed character
				Point possessedCenter = player->GetCenter();
				
				//Adjust the x value
				if (possessedCenter.X > aimingLocation.X)
				{
					aimingLocation.X += AIMING_SPEED;
				}
				else
				{
					aimingLocation.X -= AIMING_SPEED;
				}

				//Adjust the Y value
				if (possessedCenter.Y > aimingLocation.Y)
				{
					aimingLocation.Y += AIMING_SPEED;
				}
				else
				{
					aimingLocation.Y -= AIMING_SPEED;
				}

				if (InAimingZone(player->GetPossessing()))
				{
					doneAiming = true;
				}
			}
			break;
		case AutoTurretStates::ECharacterState::Firing:
			Fire();
			break;
		case AutoTurretStates::ECharacterState::Disabled:
			//Check if all the tile triggers are disabled. 
			bool allTriggered = triggers->AllTriggered();
			if (allTriggered == false)
			{
				isAlive = true;
			}
			break;
	}//End of switch statement
}//End PerformActions(player,keys)

bool AutoTurret::InAimingZone(Character^ character)
{
	//checks if the aiming point is contained within the given characters hitbox
	Rectangle charactersHitbox = character->GetHitBox();

	if (charactersHitbox.Contains(aimingLocation))
	{
		return true;
	}
	else
	{
		return false;
	}//End else
}//End InAimingZone(character)

void AutoTurret::Fire()
{
	//Fires a projectile at the aiming location
	Point myCenter = GetCenter();

	//Calculate the gradient of the line between mycenter and the aiming location
	int xVel = aimingLocation.X - myCenter.X;
	int yVel = aimingLocation.Y - myCenter.Y;

	Point velocity = Point(xVel, yVel);

	eDirection bulletDir;
	if (xVel > 0)
	{
		bulletDir = eDirection::Right;
	}
	else
	{
		bulletDir = eDirection::Left;
	}
	bullets->Add(myCenter, velocity,bulletDir);
}