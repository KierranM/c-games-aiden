#include "stdafx.h"
#include "ViewPort.h"


ViewPort::ViewPort(
		Size startSizeInTiles,
		Point startWorldLocation,
		TileMap^ startMap,
		ObjectMap^ startObjects,
		Graphics^ startCanvas
		)
{
	sizeInTiles = startSizeInTiles;
	worldLocation = startWorldLocation;
	map = startMap;
	objects = startObjects;
	canvas = startCanvas;
}//End constructor

void ViewPort::Draw()
{
	int tileSize = map->GetTileSize();
	int objTileSize = objects->GetTileSize();
	//Get the position in tiles
	int tileX = worldLocation.X / tileSize;
	int tileY = worldLocation.Y / tileSize;

	//Get the tiles offset
	int xOffset = worldLocation.X % tileSize;
	int yOffset = worldLocation.Y % tileSize;

	//Maximum X and Y
	int xLimit = tileX + sizeInTiles.Width;
	int yLimit = tileY + sizeInTiles.Height;

	//Draw the tiles to the screen
	for (int r = tileY - 1; r < yLimit + 1; r++)
	{
		for (int c = tileX - 1; c < xLimit + 1; c++)
		{
			//Make sure r is actually a row in the tilemap
			if (r >= 0 && r < map->GetRows())
			{
				//make sure c is actually a column in the tilemap
				if (c >= 0 && c < map->GetColumns())
				{
					Bitmap^ tile = map->GetMapCellBitmap(c,r);
					int xPos = ((c - tileX) * tileSize) - xOffset;
					int yPos = ((r - tileY) * tileSize) - yOffset;
					canvas->DrawImage(tile, xPos,yPos, tileSize, tileSize);
				}//End column if
			}//End row if
		}//End column for
	}//End row for

	//Draw the objects to the screen
	for (int r = tileY - 1; r < yLimit + 1; r++)
	{
		for (int c = tileX - 1; c < xLimit + 1; c++)
		{
			//Make sure r is actually a row in the object map
			if (r >= 0 && r < objects->GetRows())
			{
				//make sure c is actually a column in the object map
				if (c >= 0 && c < objects->GetColumns())
				{
					Bitmap^ tile = objects->GetMapCellBitmap(c,r);
					int xPos = ((c - tileX) * objTileSize) - xOffset;
					int yPos = ((r - tileY) * objTileSize) - yOffset;
					canvas->DrawImage(tile, xPos,yPos, objTileSize, objTileSize);
				}//End column if
			}//End row if
		}//End column for
	}//End row for
}//End Draw()

void ViewPort::Move(int x, int y)
{
	//Move the tilemap by adding x and y to its location and checking if the move is valid
	int tileSize = map->GetTileSize();
	if((worldLocation.X + x) >= 0 && (worldLocation.X + x+ (sizeInTiles.Width * tileSize)) <= map->GetWidth())
	{
		worldLocation.X += x;
	}
	if((worldLocation.Y+ y) >= 0 && (worldLocation.Y + y + (sizeInTiles.Height * tileSize)) <= map->GetHeight())
	{
		worldLocation.Y += y;
	}
}//End Move(X,Y)

void ViewPort::CenterOn(Character^ focused)
{
	//centers the viewport on the given Character
	int tileSize = map->GetTileSize();
	int halfHeight = (sizeInTiles.Height * tileSize)/TWO;
	int halfWidth = (sizeInTiles.Width * tileSize)/TWO;

	Point entityCenter = focused->GetCenter();

	//Calculate the viewports new location
	Point newWorldLocation = Point(entityCenter.X - halfWidth, entityCenter.Y - halfHeight);

	//check if the new location is valid on the x axis
	if(CheckBoundsX(newWorldLocation.X))
	{
		worldLocation.X = newWorldLocation.X;
	}

	//check if the new location is valid on the y axis
	if (CheckBoundsY(newWorldLocation.Y))
	{
		worldLocation.Y = newWorldLocation.Y;
	}
}//End CenterOn()

bool ViewPort::CheckBoundsX(int xToCheck)
{
	int tileSize = map->GetTileSize();
	//Checks if the given x value will make the viewport move out of bounds on the X axis

	//Get the position in tiles
	int tileX = xToCheck / tileSize;

	//Get the tiles offset
	int xOffset = xToCheck % tileSize;

	//Maximum X
	int xLimit = tileX + sizeInTiles.Width;

	//Added the && statement to fix going one extra column off the left
	if(tileX >= 0 && xToCheck >= 0)
	{
		if (xLimit < map->GetColumns())
		{
			//All conditions are true
			return true;
		}
	}

	//One of the above statements is false so return false
	return false;
}//End CheckBoundsX()

bool ViewPort::CheckBoundsY(int yToCheck)
{
	int tileSize = map->GetTileSize();
	//Checks if the given yValue will make the viewport move out of bounds on the Y Axis
	//Get the position in tiles
	int tileY = yToCheck / tileSize;

	//Get the tiles offset
	int yOffset = yToCheck % tileSize;

	//Maximum Y
	int yLimit = tileY + sizeInTiles.Height;

	//Added the && statement to fix going one extra row off the bottom
	if (tileY >= 0 && yToCheck >= 0)
	{
		if (yLimit < map->GetRows())
		{
			//All conditions are true
			return true;
		}
	}
		

	//One of the above statements is false so return false
	return false;
}//End CheckBoundsY()

Size ViewPort::GetSizeInPixels()
{
	//Calculates the viewports size in pixels
	int tileSize = map->GetTileSize();
	int widthInPixels = sizeInTiles.Width*tileSize;
	int heightInPixels = sizeInTiles.Height*tileSize;

	Size sizeInPixels = Size(widthInPixels,heightInPixels);
	return sizeInPixels;
}//End GetSizeInPixels()