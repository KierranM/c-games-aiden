#include "stdafx.h"
#include "Civilian.h"


Civilian::Civilian(
	array<String^, TWO>^ filenames, 
	array<int,TWO>^ frameNumbers, 
	array<Size>^ frameSize,
	Point startLocation,
	int startAction,
	int startXDeadZone,
	int startYDeadZone,
	Graphics^ startCanvas,
	Random^ startRandom,
	Rectangle startBounds,
	TileMap^ startMap,
	ObjectMap^ startObjects,
	int startWanderingTime,
	int startWanderingTicks,
	int startWorkingTime,
	int startWorkingTicks
)
:FiniteStateCharacter
(
	filenames,
	frameNumbers,
	frameSize,
	startLocation,
	startAction,
	startXDeadZone,
	startYDeadZone,
	startCanvas, 
	startRandom,
	startBounds,
	startMap,
	startObjects,
	true, 
	startWanderingTime, 
	startWanderingTicks, 
	startWorkingTime, 
	startWorkingTicks
)
{
	//Set the velocity to the defualt civilian speed
	velocity.X = CIVILIAN_MOVE_SPEED;
}

void Civilian::UpdateState(AidenCharacter^ player, BitArray^ keys){
	//The logic that updates the State of the Civilian based on events

	//Switch on the characters current action
	switch ((CivilianStates::ECharacterState)currentAction)
	{
	case CivilianStates::ECharacterState::Wandering:
		
		//Has the character wandered for too long
		if(wanderingTicks > wanderingTime || random->Next(WORK_CHANCE) == 0)
		{
			currentAction = CivilianStates::ECharacterState::Working;
			workingTicks = 0;
			System::Diagnostics::Debug::WriteLine("Civilian switching from Wandering to Working");
		}//End switch to working if

		//Is the players possessed NPC is in viewing range
		if (PossessedNPCInLOS(player, VIEW_DISTANCE, true))
		{
			currentAction = CivilianStates::ECharacterState::Fleeing;
			System::Diagnostics::Debug::WriteLine("Civilian switching from Wandering to Fleeing");
		}//End switch to fleeing if

		//Has the player been clicked then switch to possessed
		if(wasClicked)
		{
			currentAction = CivilianStates::ECharacterState::Possessed;
			isPossessed = true;
			wasClicked = false;
			System::Diagnostics::Debug::WriteLine("Civilian switching from Wandering to Possesed");
		}//End switch to possessed if

		//Is the character standing on a non-solid block
		if(StandingOnAir())
		{
			currentAction = CivilianStates::ECharacterState::Falling;
			landed = false;
			System::Diagnostics::Debug::WriteLine("Civilian switching from Wandering to Falling");
		}//End switch to falling if

		//Is the character marked as dead
		if(!isAlive)
		{
			currentAction = CivilianStates::ECharacterState::Dead;
			System::Diagnostics::Debug::WriteLine("Civilian switching from Wandering to Dead");
		}//End switch to dead if
		break;//End of Wandering case

	case CivilianStates::ECharacterState::Working:
		//Has the civilian been exercising too long
		if(workingTicks > workingTime || random->Next(WORK_CHANCE) == 0)
		{
			currentAction = CivilianStates::ECharacterState::Wandering;
			currentDirection = (eDirection)random->Next(2);
			wanderingTicks = 0;
			System::Diagnostics::Debug::WriteLine("Civilian switching from Working to Wandering");
		}//End switch to Wandering if

		//Is the players possessed NPC is in viewing range
		if (PossessedNPCInLOS(player, VIEW_DISTANCE, true))
		{
			currentAction = CivilianStates::ECharacterState::Fleeing;
			System::Diagnostics::Debug::WriteLine("Civilian switching from Working to Fleeing");
		}//End switch to fleeing if

		//If the civilian was clicked
		if(wasClicked)
		{
			isPossessed = true;
			wasClicked = false;
			currentAction = CivilianStates::ECharacterState::Possessed;
			System::Diagnostics::Debug::WriteLine("Civilian switching from Working to Possessed");
		}//End switch to possessed if

		//Is the character standing on a non-solid block
		if(StandingOnAir())
		{
			currentAction = CivilianStates::ECharacterState::Falling;
			landed = false;
			System::Diagnostics::Debug::WriteLine("Civilian switching from Working to Falling");
		}//End switch to falling if

		//Is the character marked as dead
		if(!isAlive)
		{
			currentAction = CivilianStates::ECharacterState::Dead;
			System::Diagnostics::Debug::WriteLine("Civilian switching from Working to Dead");
		}//End switch to dead if
		break;//End of Working case

	case CivilianStates::ECharacterState::Fleeing:
		//Has the player stopped possessing or has this NPC moved far enough away from the possessed NPC
		if (player->GetPossessing() == nullptr || DistanceFrom(player->GetPossessing()).distance > SAFE_DISTANCE)
		{
			currentAction = CivilianStates::ECharacterState::Wandering;
			wanderingTicks = 0;
			System::Diagnostics::Debug::WriteLine("Civilian switching from Fleeing to Wandering");
		}//End switch to switch to wandering if

		//If the player was clicked
		if(wasClicked)
		{
			isPossessed = true;
			wasClicked = false;
			currentAction = CivilianStates::ECharacterState::Possessed;
			System::Diagnostics::Debug::WriteLine("Civilian switching from Fleeing to Possessed");
		}//End switch to possessed if

		//Is the character standing on a non-solid block
		if(StandingOnAir())
		{
			currentAction = CivilianStates::ECharacterState::Falling;
			landed = false;
			System::Diagnostics::Debug::WriteLine("Civilian switching from Fleeing to Falling");
		}//End switch to falling if

		//Is the character marked as dead
		if(!isAlive)
		{
			currentAction = CivilianStates::ECharacterState::Dead;
			System::Diagnostics::Debug::WriteLine("Civilian switching from Fleeing to Dead");
		}//End switch to dead if
		break; //End of Fleeing case

	case CivilianStates::ECharacterState::Possessed:
		//If the player was clicked
		if(wasClicked)
		{
			isPossessed = false;
			wasClicked = false;
			currentAction = CivilianStates::ECharacterState::Wandering;
			System::Diagnostics::Debug::WriteLine("Civilian switching from Possessed to Wandering");
		}//End switch to Wandering if

		//Is the character standing on a non-solid block
		if(StandingOnAir())
		{
			currentAction = CivilianStates::ECharacterState::Falling;
			landed = false;
			System::Diagnostics::Debug::WriteLine("Civilian switching from Possessed to Falling");
		}//End switch to Falling if

		//Has the player pressed the spacebar
		if (keys->Get((int)Keys::Space))
		{
			landed = false;
			//Make the NPC move upwards
			velocity.Y = CIVILIAN_JUMP_STRENGTH;
			currentAction = CivilianStates::ECharacterState::Falling;
			System::Diagnostics::Debug::WriteLine("Civilian switching from Possessed to Jumping");
		}//End switch to falling upwards if

		//Is the character marked as dead
		if(!isAlive)
		{
			currentAction = CivilianStates::ECharacterState::Dead;
			System::Diagnostics::Debug::WriteLine("Civilian switching from Possessed to Dead");
		}//End switch to dead if
		break; //End of Possessed case

	case CivilianStates::ECharacterState::Falling:
		//Has the NPC landed
		if (landed)
		{
			//Is the NPC possessed
			if (isPossessed)
			{
				//Set the state to possessed
				currentAction = CivilianStates::ECharacterState::Possessed;
				System::Diagnostics::Debug::WriteLine("Civilian switching from Falling to Possessed");
			}
			else//The NPC is not possessed
			{
				//Set the state to wandering
				currentAction = CivilianStates::ECharacterState::Wandering;
				System::Diagnostics::Debug::WriteLine("Civilian switching from Falling to Wandering");
			}//End of not possessed else
		}//End of has landed if
		break; //End of Falling case

	case CivilianStates::ECharacterState::Dead:
		//Do nothing, it's dead Jim
		break;//End of Dead case
	}//End of switch
}//End of UpdateState(player, keys)

void Civilian::PerformActions(AidenCharacter^ player, BitArray^ keys){
	//Performs the actions for the Civilian based on its current state

	//Increase the number of ticks since the last interaction
	interactionTicks++;

	//Switch on the characters current action
	switch ((CivilianStates::ECharacterState)currentAction)
	{
	case CivilianStates::ECharacterState::Wandering:
		WanderPatrol();
		break;

	case CivilianStates::ECharacterState::Working:
		WorkPaused();
		break;

	case CivilianStates::ECharacterState::Fleeing:
		Flee(player);
		break;

	case CivilianStates::ECharacterState::Possessed:
		PossessedActions(keys);
		break;

	case CivilianStates::ECharacterState::Falling:
		Fall(keys);
		break;

	case CivilianStates::ECharacterState::Dead:
		//Adjust the character to be in the right place
		if (!deathAdjustment)
		{
			OnDeath();
		}
		//Do nothing, it's dead Jim
		break;
	}//End of Switch
}//End of PerformActions(player,keys)

void Civilian::Flee(AidenCharacter^ player)
{
	//Makes the Civilian move away from the possessed NPC	

	//Is the player still possessing an NPC
	if(player->GetPossessing() != nullptr)
	{
		//Move the civilian in the opposite direction to aiden
		Distance d = DistanceFrom(player->GetPossessing());

		//Is the possessed NPC to the left
		if (d.direction == eDirection::Left)
		{
			//Then move right
			currentDirection = eDirection::Right;
		}
		else//Its to the right
		{
			//So move left
			currentDirection = eDirection::Left;
		}

		//Do the move
		Move();
	}//End of is possessing if
}//End of Flee(player)
