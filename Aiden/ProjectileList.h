#pragma once
#include "Projectile.h"

/*
	Author: Kierran McPherson
	Date: 18/11/2013
	Purpose: A LinkedList of Projectiles that allows mass control over many Projectiles at once.
			 It also has the ability to directly add projectiles to itself given a location, velocity, and direction
	Known Bugs:
*/

ref class ProjectileList
{
private:
	//For Projectile creation
	TileMap^ map;
	ObjectMap^ objects;
	Graphics^ canvas;
	String^ imageFilepath;
	Rectangle world;
	
	//LinkedList fields
	Projectile^ head;
	Projectile^ tail;
public:
	ProjectileList();
	ProjectileList(Projectile^ first);
	ProjectileList(TileMap^ startMap, ObjectMap^ startObjects, String^ startImageFilepath, Graphics^ startCanvas, Rectangle startWorld);

	int Count();
	void Add(Projectile^ node);
	void Add(Point location, Point velocity, eDirection direction);
	void Remove(Projectile^ node);
	void DrawAll(Point viewportWorldPosition, Size viewportSize);
	void UpdateAll();
	void CollisionsWith(Character^ character);
	bool InViewport(Point projectileLocation, Size projectileSize, Size viewportSize);
};

