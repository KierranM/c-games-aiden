#include "stdafx.h"
#include "Projectile.h"


Projectile::Projectile(
	Point startLocation,
	Point startVelocity,
	eDirection startDirection,
	Rectangle startBoundary,
	TileMap^ startMap,
	ObjectMap^ startObjects,
	Graphics^ startCanvas,
	String^ imageFileName
	)
{
	//Set up the projectile using the given values
	location = startLocation;
	velocity = startVelocity;
	horizontalDirection = startDirection;
	boundary = startBoundary;
	map = startMap;
	objects = startObjects;
	canvas = startCanvas;
	isAlive = true;
	image = gcnew Bitmap(imageFileName);
	//Make the projectiles image transparent
	image->MakeTransparent(image->GetPixel(0,0));
}//End constructor 8 parameters

void Projectile::Draw(Point viewportWorldLocation)
{
	//Draw the projectile relative to the current viewport world location
	int relativeX = location.X - viewportWorldLocation.X;
	int relativeY = location.Y - viewportWorldLocation.Y;
	canvas->DrawImage(image,relativeX,relativeY);
}//End Draw(viewportWorldLocation)

void Projectile::Move()
{
	//Calculate the projectiles next location and check if it is valid. If it is move, otherwise set the projectile to be dead
	Size s = image->Size;
	Rectangle temp = Rectangle(location,s);

	//check which direction the projectile is heading
	if (horizontalDirection == eDirection::Left)
	{
		temp.X -= velocity.X;
	}
	else //It is heading right
	{
		temp.X += velocity.X;
	}
	//Add the y velocity to the temp location. May add gravity to projectiles later. But at the moment they are traveling so fast it doesn't matter
	temp.Y += velocity.Y;

	//check if the projectile is hitting a solid tile
	if (CheckTile(temp)|| OutOfBounds(temp))
	{
		//Kill the projectile
		isAlive = false;
	}
	else//The move is valid
	{
		location = temp.Location;
	}
}//End Move()

bool Projectile::CheckTile(Rectangle toCheck)
{
	//The projectile is small enough to only worry about checking the tile at the current location

	//Get the tiles size
	int tileSize = map->GetTileSize();
	int tileY = toCheck.Y/tileSize;
	int tileX = toCheck.X/tileSize;

	bool solidTile = false;

	//Check if the tile in the map or object map is solid
	if (map->GetIsSolid(tileX,tileY) || objects->GetIsSolid(tileX,tileY))
	{
		solidTile = true;
	}
	return solidTile;
}//End CheckTile(toCheck)

bool Projectile::OutOfBounds(Rectangle toCheck)
{
	//Check if the given rectangle is outside of the boundary rectangle
	if ((toCheck.Right < boundary.X || toCheck.X > boundary.Right) || (toCheck.Bottom < boundary.Y || toCheck.Y > boundary.Bottom))
	{
		return true;
	}
	else //It is out of bounds
	{
		return false;
	}
}//End OutOfBounds(toCheck)

bool Projectile::CollidesWith(Character^ character)
{
	bool collides = false;
	//Calculate the projectiles hitbox
	Size s = image->Size;
	Rectangle temp = Rectangle(location,s);

	//Get the hitbox of the character we wish to check
	Rectangle charactersHitBox = character->GetHitBox();

	//Check if the two rectangles intersect
	if (charactersHitBox.IntersectsWith(temp))
	{
		//The two have collided
		collides = true;
	}
	
	//If the bullet does not directly collide with the character
	if (!collides)
	{
		int count = 0;
		bool hitWall = false;
		
		//Get the variables that wont change in the loop
		int tileSize = map->GetTileSize();
		int tileY = location.Y/tileSize;
		//Extend a line of movement to see if any of the space the bullet will skip contains the character
		while(count < velocity.X && !collides && !hitWall) 
		{
			Point p;

			if (horizontalDirection == eDirection::Left)
			{
				p = Point(location.X - count, location.Y);
			}
			else
			{
				p = Point(location.X + count, location.Y);
			}

			
			//Dont count anything beyond a wall
			int tileX = p.X/tileSize;

			//Check if the tile in the map or object map is solid
			if (map->GetIsSolid(tileX,tileY) || objects->GetIsSolid(tileX,tileY))
			{
				hitWall = true;
			}

			//Check if the point is inside the characters hitbox
			if(charactersHitBox.Contains(p))
			{
				collides = true;
			}
			count++;
		} //End while
	}//End if

	return collides;
}//End CollidesWith(character)