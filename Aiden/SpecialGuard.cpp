#include "stdafx.h"
#include "SpecialGuard.h"


SpecialGuard::SpecialGuard(
		array<String^, TWO>^ filenames, 
		array<int,TWO>^ frameNumbers, 
		array<Size>^ frameSize,
		Point startLocation,
		int startAction,
		int startXDeadZone,
		int startYDeadZone,
		Graphics^ startCanvas,
		Random^ startRandom,
		Rectangle startBounds,
		TileMap^ startMap,
		ObjectMap^ startObjects,
		int startPatrollingTime,
		int startPatrollingTicks,
		int startPausedTime,
		int startPausedTicks,
		ProjectileList^ startBullets
	)
	:Guard(
		filenames,
		frameNumbers,
		frameSize,
		startLocation,
		startAction,
		startXDeadZone,
		startYDeadZone,
		startCanvas,
		startRandom,
		startBounds,
		startMap,
		startObjects,
		startPatrollingTime,
		startPatrollingTicks,
		startPausedTime,
		startPausedTicks,
		false,
		startBullets
	)
{
	//Set up the special guard using the default values
	velocity.X = SPECIAL_GUARD_SPEED;
}//End constructor 17 params

void SpecialGuard::UpdateState(AidenCharacter^ player, BitArray^ keys){
	//The logic that updates the State of the SpecialGuard based on events

	//Switch on the characters current action
	switch ((SpecialGuardStates::ECharacterState)currentAction)
	{
	case SpecialGuardStates::ECharacterState::Patrolling:
		
		//Has the character wandered for too long
		if(wanderingTicks > wanderingTime || random->Next(PAUSE_CHANCE) == 0)
		{
			currentAction = SpecialGuardStates::ECharacterState::Paused;
			workingTicks = 0;
			System::Diagnostics::Debug::WriteLine("SpecialGuard switching from Patrolling to Paused");
		}//End of switch to paused if

		//Is the players possessed NPC is in viewing range
		if (PossessedNPCInLOS(player, SPECIAL_GUARD_VIEW_DISTANCE, true) && player->GetIsAlive() == true)
		{
			currentAction = SpecialGuardStates::ECharacterState::Pursuing;
			System::Diagnostics::Debug::WriteLine("SpecialGuard switching from Patrolling to Pursuing");
		}//End of switch to pursuing if

		//Is the character standing on a non-solid block
		if(StandingOnAir())
		{
			currentAction = SpecialGuardStates::ECharacterState::Falling;
			landed = false;
			System::Diagnostics::Debug::WriteLine("SpecialGuard switching from Patrolling to Falling");
		}//End of switch to Falling if

		break; //End of Patrolling case

	case SpecialGuardStates::ECharacterState::Paused:
		//Has the civilian been exercising too long
		if(workingTicks > workingTime || random->Next(PAUSE_CHANCE) == 0)
		{
			currentAction = SpecialGuardStates::ECharacterState::Patrolling;
			currentDirection = (eDirection)random->Next(2);
			wanderingTicks = 0;
			System::Diagnostics::Debug::WriteLine("SpecialGuard switching from Paused to Patrolling");
		}//End of switch to Patrolling if

		//Is the players possessed NPC is in viewing range
		if (PossessedNPCInLOS(player, SPECIAL_GUARD_VIEW_DISTANCE, true) && player->GetIsAlive() == true)
		{
			currentAction = SpecialGuardStates::ECharacterState::Pursuing;
			System::Diagnostics::Debug::WriteLine("SpecialGuard switching from Paused to Pursuing");
		}//End of switch to Pursuing if

		//Is the character standing on a non-solid block
		if(StandingOnAir())
		{
			currentAction = SpecialGuardStates::ECharacterState::Falling;
			landed = false;
			System::Diagnostics::Debug::WriteLine("SpecialGuard switching from Paused to Falling");
		}//End of switch to Falling if

		break;//End of Paused case

	case SpecialGuardStates::ECharacterState::Pursuing:
		//If the guard has lost sight of the possessed NPC
		if (player->GetPossessing() == nullptr || player->GetIsAlive() == false || DistanceFrom(player->GetPossessing()).distance > SAFE_DISTANCE)
		{
			currentAction = SpecialGuardStates::ECharacterState::Patrolling;
			wanderingTicks = 0;
			System::Diagnostics::Debug::WriteLine("SpecialGuard switching from Pursuing to Patrolling");
		}//End of switch to Patrolling if

		//If the possessed NPC is within the range of engagement
		if ((player->GetIsAlive() == true && player->GetPossessing() != nullptr) && DistanceFrom(player->GetPossessing()).distance < ENGAGE_DISTANCE)
		{
			currentAction = SpecialGuardStates::ECharacterState::Engaging;
			System::Diagnostics::Debug::WriteLine("SpecialGuard switching from Pursuing to Engaging");
		}//End of switch to Engaging if

		//Is the character standing on a non-solid block
		if(StandingOnAir())
		{
			currentAction = SpecialGuardStates::ECharacterState::Falling;
			landed = false;
			System::Diagnostics::Debug::WriteLine("SpecialGuard switching from Pursuing to Falling");
		}//End of switch to Falling if

		break;//End of Pursuing case

	case SpecialGuardStates::ECharacterState::Engaging:

		//If the guard has lost sight of the possessed NPC OR  it is no longer possessed or it is dead
		if (player->GetPossessing() == nullptr || player->GetIsAlive() == false || DistanceFrom(player->GetPossessing()).distance > SAFE_DISTANCE)
		{
			currentAction = SpecialGuardStates::ECharacterState::Patrolling;
			wanderingTicks = 0;
			System::Diagnostics::Debug::WriteLine("SpecialGuard switching from Engaging to Patrolling");
		}//End of switch to Patrolling if

		//If the possessed NPC is outside of the range of engagement
		if (player->GetPossessing() != nullptr && DistanceFrom(player).distance > ENGAGE_DISTANCE)
		{
			currentAction = SpecialGuardStates::ECharacterState::Pursuing;
			System::Diagnostics::Debug::WriteLine("SpecialGuard switching from Engaging to Pursuing");
		}//End of switch to Pursuing if

		//Is the character standing on a non-solid block
		if(StandingOnAir())
		{
			currentAction = SpecialGuardStates::ECharacterState::Falling;
			landed = false;
			System::Diagnostics::Debug::WriteLine("SpecialGuard switching from Engaging to Falling");
		}//End of switch to Falling if

		break;//End of Engaging case

	case SpecialGuardStates::ECharacterState::Falling:
		//Has the SpecialGuard landed
		if (landed)
		{
			currentAction = SpecialGuardStates::ECharacterState::Patrolling;
			System::Diagnostics::Debug::WriteLine("SpecialGuard switching from Falling to Patrolling");
		}//End of switch to Patrolling or Possessed if
		break;//End of Falling case
	}//End of switch statement
}//End of UpdateState(player,keys)

void SpecialGuard::PerformActions(AidenCharacter^ player, BitArray^ keys){
	//Performs specific actions based on the current state

	//Increase the number of ticks since the last interaction
	interactionTicks++;

	//Switch on the characters current action
	Rectangle temp = hitbox;
	switch ((SpecialGuardStates::ECharacterState)currentAction)
	{
	case SpecialGuardStates::ECharacterState::Patrolling:
		WanderPatrol();
		break;

	case SpecialGuardStates::ECharacterState::Paused:
		WorkPaused();
		break;

	case SpecialGuardStates::ECharacterState::Pursuing:
		Pursue(player);
		break;

	case SpecialGuardStates::ECharacterState::Engaging:
		Engage();
		break;

	case SpecialGuardStates::ECharacterState::Falling:
		Fall(keys);
		break;
	}//End of switch statement
}//End of PerformActions(player,keys)

