#include "stdafx.h"
#include "AidenCharacter.h"


AidenCharacter::AidenCharacter(
		array<String^, TWO>^ filenames, 
		array<int,TWO>^ frameNumbers, 
		array<Size>^ frameSize,
		Point startLocation,
		int startAction,
		int startXDeadZone,
		int startYDeadZone,
		Graphics^ startCanvas,
		Random^ startRandom,
		Rectangle startBounds,
		TileMap^ startMap,
		ObjectMap^ startObjects
	)
	:Character(
		filenames,
		frameNumbers,
		frameSize,
		startLocation,
		startAction, 
		NUM_DIRECTIONS,
		startXDeadZone,
		startYDeadZone,
		startCanvas, 
		startRandom,
		startBounds,
		startMap,
		startObjects
	)
{
	//Set up the Aiden Character by passing all the information to the Character constructor
}//End constructor 12 params

void AidenCharacter::Update()
{
	//Update Aiden
	spriteSheets[currentAction, currentDirection]->UpdateCurrentFrame();

	//If the possessed NPC has died
	if (possessing != nullptr && possessing->GetIsAlive() == false)
	{
		isAlive = false;
	}//end if
}//End Update()

void AidenCharacter::Move()
{
	//Only move Aiden if he is not possessing someone
	if (possessing == nullptr)
	{
		//Call the base classes move
		Character::Move();
	}//End if
}//End Move()

void AidenCharacter::CenterOn(Character^ otherGuy)
{
	//Moves the AidenCharacters center to be the same as that of the passed in character

	//Get the otherGuys center
	Point ogCenter = otherGuy->GetCenter();
	
	//Get the current frame size
	Size frameSize = spriteSheets[currentAction,currentDirection]->GetFrameSize();

	//Calculate the new location
	int locX = ogCenter.X - (frameSize.Width/TWO);
	int locY = (ogCenter.Y - (frameSize.Height/TWO)) - UPWARDS_SHIFT;
	//Set the new location
	location = Point(locX,locY);

	//Adjust the hitboxes location
	int hitX = locX + xDeadzone;
	int hitY = locY +yDeadzone;
	hitbox.Location = Point(hitX,hitY);
}//End CenterOn(otherGuy)

bool AidenCharacter::CheckTileX(Rectangle toCheck)
{
	//Checks if the given rectangle overlaps any tiles that are barriers to Aiden on its vertical (left and right) edges

	//Get the maps tile size
	int tileSize = map->GetTileSize();

	//Create an array of Points that to hold all the points along the edge of the rectangle
	array<Point>^ sidePoints = gcnew array<Point>(toCheck.Height);

	//Loop for the height of the rectangle and add the points to the array
	for (int i = 0; i < toCheck.Height; i++)
	{
		sidePoints[i] = Point(toCheck.X, toCheck.Y + i);
	}

	bool solidTile = false;
	
	//Loop through each of the points set up above
	for (int i = 0; i < toCheck.Height; i++)
	{
		//Calculate the indices of the tile at the current point on the left and right sides of the character
		int tileX = sidePoints[i].X/tileSize;
		int tileY = sidePoints[i].Y/tileSize;

		int rightTileX = (sidePoints[i].X + toCheck.Width)/tileSize;

		//check if the tiles or objects at that index are a barrier to Aiden
		if(map->GetIsAidenBarrier(tileX,tileY) || map->GetIsAidenBarrier(rightTileX,tileY) || (objects->GetIsAidenBarrier(tileX, tileY) || objects->GetIsAidenBarrier(rightTileX, tileY)))
		{
			solidTile = true;
		}//End if
	}//End for

	return solidTile;
}//End CheckTileX(toCheck)

bool AidenCharacter::CheckTileY(Rectangle toCheck)
{
	//Checks if the given rectangle overlaps any tiles that are barriers to Aiden on its horizontal (top and bottom) edges
 
	//Get the size of the tiles in the map
	int tileSize = map->GetTileSize();

	//Create an array to hold all the points along the rectangles edge
	array<Point>^ sidePoints = gcnew array<Point>(toCheck.Width);
	
	//loop through the array filling it with the points along the edge of the rectangle
	for (int i = 0; i < toCheck.Width; i++)
	{
		sidePoints[i] = Point(toCheck.X + i, toCheck.Y);
	}

	bool solidTile = false;
	
	//Loop through the array of points
	for (int i = 0; i < toCheck.Width; i++)
	{
		//Get the indices of the tiles on the top and bottom of the AidenCharacter
		int tileX = sidePoints[i].X/tileSize;
		int tileY = sidePoints[i].Y/tileSize;

		int rightTileY = (sidePoints[i].Y + toCheck.Height)/tileSize;

		//Check if the tile or object at the top or bottom is a barrier to Aiden
		if(map->GetIsAidenBarrier(tileX,tileY) || map->GetIsAidenBarrier(tileX,rightTileY) || (objects->GetIsAidenBarrier(tileX, tileY) || objects->GetIsAidenBarrier(tileX, rightTileY)))
		{
			solidTile = true;
		}//End if
	}//End for

	return solidTile;
}//End CheckTileY(toCheck)

void AidenCharacter::Draw(Point viewportWorldLocation)
{
	//Aiden is only visible when not possessing someone
	if (possessing == nullptr)
	{
		//Get the tile at the center of the AidenCharacter
		Point center = GetCenter();
		int centerTileX = center.X/map->GetTileSize();
		int centerTileY = center.Y/map->GetTileSize();

		Point characterViewportLocation = CalculateViewPortLocation(viewportWorldLocation);


		//Draws the AidenCharacter at the given location
		canvas->DrawImage(spriteSheets[currentAction, currentDirection]->GetFrame(), characterViewportLocation.X, characterViewportLocation.Y);

		//Display the AidenCharacters hitbox when in debug mode			
		#ifdef _DEBUG
			canvas->DrawRectangle(Pens::Black, characterViewportLocation.X + xDeadzone, characterViewportLocation.Y + yDeadzone, hitbox.Width, hitbox.Height);
		#endif
		
	}//end is possessing if
}//End Draw(viewportWorldLocation)

Point AidenCharacter::GetCenter()
{
	//Gets the center point of Aiden if he is not possessing anyone, else it gets the center point of the possessed NPC
	if (possessing == nullptr)
	{
		//Gets the mid point of the character
		Size currentFrameSize = spriteSheets[currentAction,0]->GetFrameSize();
		int x = location.X + currentFrameSize.Width/TWO;
		int y = location.Y + currentFrameSize.Height/TWO;

		return Point(x,y); 
	}
	else//Get the center point of the possessed NPC
	{
		return possessing->GetCenter();
	}//end else
}//end GetCenter()

bool AidenCharacter::IsGameOver()
{
	//Checks if the character Aiden is possessing is dead
	if (possessing != nullptr)
	{
		//If the possessed character is dead
		if (!possessing->GetIsAlive())
		{
			//The game is over as the possessed NPC died while Aiden was possessing it
			return true;
		}//End possessed NPC alive if
	}//End is possessing if

	//The game is not over
	return false;
}//End IsGameOver()

bool AidenCharacter::CheckForWin()
{
	//Checks if Aiden's center or his possessed NPC's center is on an active victory tile

	Point center;
	//If Aiden is not possessing someone
	if (possessing == nullptr)
	{
		//Get Aidens center
		center = GetCenter();
	}
	else//Aiden is possessing someone
	{
		center = possessing->GetCenter();
	}

	//Calculate the object tile's index
	int tileSize = objects->GetTileSize();
	int tileX = center.X/tileSize;
	int tileY = center.Y/tileSize;

	if (objects->GetIsVictoryTile(tileX,tileY))
	{
		//The object tile was an active victory tile
		return true;
	}
	else
	{
		//It was not an active victory tile
		return false;
	}
}