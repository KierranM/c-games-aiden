#include "stdafx.h"
#include "TriggerList.h"
#include "AnimatedTile.h"


TriggerList::TriggerList(void)
{
	//Make both pointers point to nothing
	head = nullptr;
	tail = nullptr;
}//End constructor no params


TriggerList::TriggerList(AnimatedTile^ first){
	//Set both pointers to point to the first AnimatedTile
	head = first;
	tail = first;
}//End constructor 1 params


int TriggerList::Count(){
	//Loops through the list and counts the number of AnimatedTiles
	int count = 0;
	AnimatedTile^ nodeWalker = head;
	while(nodeWalker != nullptr){
		nodeWalker = nodeWalker->Next;
		count++;
	}

	return count;
}//End Count()


void TriggerList::Add(AnimatedTile ^node){
	//Adds a trigger to the list
	if(tail == nullptr){ //Empty list
		head = node;
		tail = node;
	}
	else{ //Adding to end
		tail->Next = node;
		tail = node;
	}
}//End Add()


void TriggerList::Remove(AnimatedTile ^node){
	AnimatedTile^ nodeWalker;

	//start at the beginning of the list
	nodeWalker = head;

	//If the node to be deleted is the head
	if(node == head){
		//If the node is the head and has no next
		if(node->Next == nullptr){
			head = nullptr;
			tail = nullptr;
		}
		else{ //Node is the first but not the only item in the list
			head = head->Next;
		}
	}
	else{ //The node to be deleted are not head
		AnimatedTile^ temp;
		while(nodeWalker->Next != node){
			nodeWalker = nodeWalker->Next;
		}
		//Store the node
		temp = nodeWalker->Next;
		//Check if the node that is being removed is the tail
		if(temp == tail){
			//Make the tail the current node and set its next to nothing
			tail = nodeWalker;
			nodeWalker->Next = nullptr;
		}
		else{
			//Otherwise make the list skip over the node that is being removed
			nodeWalker->Next = temp->Next;
		}
	}//End of node not head if
}//End Remove()

bool TriggerList::AllTriggered()
{
	//Checks if all the triggers in this list are set to true. If even a single one isn't then return false
	bool all = true;
	AnimatedTile^ nodeWalker = head;

	if(nodeWalker != nullptr){

		while(nodeWalker != nullptr){

			//Check if the node is enabled
			if (nodeWalker->GetEnabled() == false)
			{
				all = false;
			}
			nodeWalker = nodeWalker->Next;
		}
	}

	return all;
}//End AllTriggered()
