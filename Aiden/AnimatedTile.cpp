#include "stdafx.h"
#include "AnimatedTile.h"
#include "TriggerList.h"


AnimatedTile::AnimatedTile(
		String^ mainImageFile, 
		String^ altImageFile, 
		bool startEnabled, 
		bool startSolidWhenEnabled, 
		bool startLethal, 
		bool startIsSolid, 
		bool startAidenBarrier, 
		bool startCanBeClicked,
		bool startVictoryTile
	)
	:Tile(
		mainImageFile, 
		startLethal, 
		startIsSolid,
		startAidenBarrier
	)
{
	//Set up the fields using the given values
	alternateTileImage = gcnew Bitmap(altImageFile);
	enabled = startEnabled;
	solidWhenEnabled = startSolidWhenEnabled;
	canBeClicked = startCanBeClicked;
	isVictoryTile = startVictoryTile;
	//Create the list of triggers
	triggers = gcnew TriggerList();
}//End constructor 8 params

Bitmap^ AnimatedTile::GetTileImage()
{
	//If the animated tile is not enabled then return it's main image
	if (!enabled)
	{
		return tileImage;
	}
	else //Return the other image
	{
		return alternateTileImage;
	}
}//End GetTileImage()

void AnimatedTile::SetTileImage(Bitmap^ main, Bitmap^ second)
{
	//Overloads the base classes SetTileImage method

	tileImage = main;
	alternateTileImage = second;
}//End SetTileImage(main,second)

void AnimatedTile::AddTrigger(AnimatedTile^ trigger)
{
	//Add the given tile as a trigger to this tiles trigger list
	triggers->Add(trigger);
}//End AddTrigger(trigger)

void AnimatedTile::Trigger()
{
	//Flip the value of enabled
	enabled = !enabled;
}//End Trigger()

void AnimatedTile::UpdateState()
{
	//Updates the state of this AnimatedTile if all of its triggers are enabled

	//If the AnimatedTile has any triggers
	if (triggers->Count() > 0)
	{
		bool allTriggersEnabled = triggers->AllTriggered();

		//If all the AnimatedTiles triggers are enables
		if (allTriggersEnabled)
		{
			enabled = true;
		}
		else//Not all triggers are enabled
		{
			enabled = false;
		}//end else
	}//End no triggers if
}//End UpdateState()

bool AnimatedTile::GetIsSolid()
{
	//Gets if the AnimatedTile is solid depending on its solidWhenEnabled field
	bool solid = isSolid;

	//If the object is not solid when enabled then return false
	if ((!solidWhenEnabled) && (enabled))
	{
		solid = false;
	}
	else //It is not enabled
	{
		//If the object IS solid when enabled, but is not enabled, and is usually solid
		if ((!solidWhenEnabled) && (!enabled) && (!isSolid))
		{
			solid = false;
		}//End if
	}//End else

	return solid;
}//End GetIsSolid()

bool AnimatedTile::GetIsAidenBarrier()
{
	//Gets if the AnimatedTile is a barrier to Aiden depending on its solidWhenEnabled field
	bool barrier = isAidenBarrier;

	//If the object is not solid when enabled then return false
	if ((!solidWhenEnabled) && (enabled))
	{
		barrier = false;
	}
	else//It is not enabled
	{
		//If the object IS solid when enabled, but is not enabled, and is usually a barrier
		if ((!solidWhenEnabled) && (!enabled) && (!isAidenBarrier))
		{
			barrier = false;
		}//End if
	}//end else

	return barrier;
}//End GetIsAidenBarrier()

void AnimatedTile::SetTileTransparency(Color transparencyColor)
{
	//Set the transparency on both images to the given colour
	tileImage->MakeTransparent(transparencyColor);
	alternateTileImage->MakeTransparent(transparencyColor);
}//End SetTileTransparency()

bool AnimatedTile::GetIsVictoryTile()
{
	//Check if this tile is a victory tile, victory tiles only work when enabled

	//Is the tile enabled
	if (enabled)
	{
		if (isVictoryTile)
		{
			//The tile is enabled and is a victory tile
			return true;
		}
	}

	//The tile either wasn't enabled or wasn't a victory tile
	return false;
}//End GetIsVictoryTile()