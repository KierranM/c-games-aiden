#pragma once
#include "TileMap.h"
#include "ObjectMap.h"
#include "Character.h"
#define TWO 2

	using namespace System;
	using namespace System::ComponentModel;
	using namespace System::Collections;
	using namespace System::Windows::Forms;
	using namespace System::Data;
	using namespace System::Drawing;

/*
	Author: Kierran McPherson
	Date: 18/11/2013
	Purpose: This class defines the ViewPort which is used to display the visible section of the world.
	Known Bugs:
*/

ref class ViewPort
{
private:
	Size sizeInTiles;
	Point worldLocation;
	TileMap^ map;
	ObjectMap^ objects;
	Graphics^ canvas;

public:
	ViewPort(
		Size startSizeInTiles,
		Point startWorldLocation,
		TileMap^ startMap,
		ObjectMap^ startObjects,
		Graphics^ startCanvas
	);

	void Draw();
	void Move(int x, int y);
	void CenterOn(Character^ focused);
	bool CheckBoundsX(int xToCheck);
	bool CheckBoundsY(int yToCheck);
	
	//Gets and Sets
	Point GetWorldLocation()	{return worldLocation;}
	void SetWorldLocation(Point worldLoc)	{worldLocation = worldLoc;}

	Size GetSizeInTiles()	{return sizeInTiles;}
	void SetSizeInTiles(int rows, int cols)	{sizeInTiles = Size(rows, cols);}

	Size GetSizeInPixels();
};