#pragma once
#include "TileList.h"
	using namespace System;
	using namespace System::IO;
	using namespace System::ComponentModel;
	using namespace System::Collections;
	using namespace System::Windows::Forms;
	using namespace System::Data;
	using namespace System::Drawing;

/*
	Author: Kierran McPherson
	Date: 18/11/2013
	Purpose: This class defines a TileMap that contains an array that acts as a map of the different types of tiles in the world
	Known Bugs:
*/
ref class TileMap
{
private:
	array<int, 2>^ map;
	int rows;
	int columns;
	int tileSize;
	TileList^ tiles;
	Graphics^ canvas;

public:
	TileMap(int startRows, int startCols, int startTileSize, TileList^ startList, Graphics^ startCanvas);

	void Draw();
	Bitmap^ GetMapCellBitmap(int c, int r);
	void LoadFromFile(String^ filename);

	//Gets and sets

	int GetRows()	{return rows;}
	int GetColumns()	{return columns;}
	int GetWidth()	{return columns * tileSize;}
	int GetHeight()	{return rows * tileSize;}

	int GetTileSize()	{return tileSize;}

	bool GetIsSolid(int xIndex, int yIndex)	{return tiles->GetIsSolid(map[yIndex,xIndex]);}
	bool GetIsLethal(int xIndex, int yIndex)	{return tiles->GetIsLethal(map[yIndex,xIndex]);}
	bool GetIsAidenBarrier(int xIndex, int yIndex)	{return tiles->GetIsAidenBarrier(map[yIndex,xIndex]);}

	int GetTileType(int xIndex, int yIndex)	{return map[yIndex, xIndex];}
	void SetTileType(int xIndex, int yIndex, int type) {map[yIndex, xIndex] = type;}
};

