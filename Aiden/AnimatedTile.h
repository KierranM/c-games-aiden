#pragma once
//Prevent compiler loops
#ifndef ANIMATED_TILE_INCLUDED
#define ANIMATED_TILE_INCLUDED
#include "Tile.h"

//Predeclaration to prevent compiler loops
ref class TriggerList;

/*
	Author: Kierran McPherson
	Date: 18/11/2013
	Purpose: This class defines an AnimatedTile which is a descendant of Tile that can be displayed differently, and has more options
	Known Bugs:
*/

ref class AnimatedTile : public Tile
{

private:
	Bitmap^ alternateTileImage;
	bool enabled;
	bool solidWhenEnabled;
	bool canBeClicked;
	bool isVictoryTile;
	TriggerList^ triggers;
public:
	AnimatedTile^ Next;
public:
	AnimatedTile(String^ mainImageFile, String^ altImageFile, bool startEnabled, bool startSolidWhenEnabled, bool startLethal, bool startIsSolid, bool startAidenBarrier, bool startCanBeClicked, bool startVictoryTile);
	
	void AddTrigger(AnimatedTile^ trigger);
	void UpdateState();
	virtual void SetTileTransparency(Color transparentColor) override;
	void Trigger();

	//gets and sets
	bool GetEnabled()	{return enabled;}
	virtual bool GetIsSolid() override;
	virtual bool GetIsAidenBarrier() override;
	
	virtual Bitmap^ GetTileImage() override;
	void SetTileImage(Bitmap^ main, Bitmap^ second);

	bool GetCanBeClicked()	{return canBeClicked;}
	bool GetIsVictoryTile();
};

#endif
