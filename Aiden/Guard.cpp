#include "stdafx.h"
#include "Guard.h"


Guard::Guard(
	array<String^, TWO>^ filenames, 
	array<int,TWO>^ frameNumbers, 
	array<Size>^ frameSize,
	Point startLocation,
	int startAction,
	int startXDeadZone,
	int startYDeadZone,
	Graphics^ startCanvas,
	Random^ startRandom,
	Rectangle startBounds,
	TileMap^ startMap,
	ObjectMap^ startObjects,
	int startPatrollingTime,
	int startPatrollingTicks,
	int startPausedTime,
	int startPausedTicks,
	bool startCanBePossessed,
	ProjectileList^ startBullets
)
:FiniteStateCharacter
(
	filenames,
	frameNumbers,
	frameSize,
	startLocation,
	startAction,
	startXDeadZone,
	startYDeadZone,
	startCanvas, 
	startRandom,
	startBounds,
	startMap,
	startObjects,
	startCanBePossessed, 
	startPatrollingTime, 
	startPatrollingTicks, 
	startPausedTime, 
	startPausedTicks
)
{
	velocity.X = GUARD_MOVE_SPEED;
	bullets = startBullets;
}

void Guard::UpdateState(AidenCharacter^ player, BitArray^ keys){
	//The logic that updates the State of the Guard based on events

	//Switch on the characters current action
	switch ((GuardStates::ECharacterState)currentAction)
	{
	case GuardStates::ECharacterState::Patrolling:
		
		//Has the character wandered for too long
		if(wanderingTicks > wanderingTime || random->Next(PAUSE_CHANCE) == 0)
		{
			currentAction = GuardStates::ECharacterState::Paused;
			workingTicks = 0;
			System::Diagnostics::Debug::WriteLine("Guard switching from Patrolling to Paused");
		}//End of switch to paused if

		//Is the players possessed NPC is in viewing range
		if (PossessedNPCInLOS(player, GUARD_VIEW_DISTANCE, true) && player->GetIsAlive() == true)
		{
			currentAction = GuardStates::ECharacterState::Pursuing;
			System::Diagnostics::Debug::WriteLine("Guard switching from Patrolling to Pursuing");
		}//End of switch to pursuing if

		//Has the Guard been clicked
		if(wasClicked)
		{
			currentAction = GuardStates::ECharacterState::Possessed;
			isPossessed = true;
			wasClicked = false;
			System::Diagnostics::Debug::WriteLine("Guard switching from Patrolling to Possesed");
		}//End of switch to Possessed if

		//Is the character standing on a non-solid block
		if(StandingOnAir())
		{
			currentAction = GuardStates::ECharacterState::Falling;
			landed = false;
			System::Diagnostics::Debug::WriteLine("Guard switching from Patrolling to Falling");
		}//End of switch to Falling if

		//Is the character marked as dead
		if(!isAlive)
		{
			currentAction = GuardStates::ECharacterState::Dead;
			System::Diagnostics::Debug::WriteLine("Guard switching from Patrolling to Dead");
		}//End of switch to Dead if

		break; //End of Patrolling case

	case GuardStates::ECharacterState::Paused:
		//Has the civilian been exercising too long
		if(workingTicks > workingTime || random->Next(PAUSE_CHANCE) == 0)
		{
			currentAction = GuardStates::ECharacterState::Patrolling;
			currentDirection = (eDirection)random->Next(2);
			wanderingTicks = 0;
			System::Diagnostics::Debug::WriteLine("Guard switching from Paused to Patrolling");
		}//End of switch to Patrolling if

		//Is the players possessed NPC is in viewing range
		if (PossessedNPCInLOS(player, GUARD_VIEW_DISTANCE, true) && player->GetIsAlive() == true)
		{
			currentAction = GuardStates::ECharacterState::Pursuing;
			System::Diagnostics::Debug::WriteLine("Guard switching from Paused to Pursuing");
		}//End of switch to Pursuing if

		//If the player was clicked
		if(wasClicked)
		{
			isPossessed = true;
			wasClicked = false;
			currentAction = GuardStates::ECharacterState::Possessed;
			System::Diagnostics::Debug::WriteLine("Guard switching from Paused to Possessed");
		}//End of switch to Possessed if

		//Is the character standing on a non-solid block
		if(StandingOnAir())
		{
			currentAction = GuardStates::ECharacterState::Falling;
			landed = false;
			System::Diagnostics::Debug::WriteLine("Guard switching from Paused to Falling");
		}//End of switch to Falling if

		//Is the character marked as dead
		if(!isAlive)
		{
			currentAction = GuardStates::ECharacterState::Dead;
			System::Diagnostics::Debug::WriteLine("Guard switching from Paused to Dead");
		}//End of switch to Dead if
		break;//End of Paused case

	case GuardStates::ECharacterState::Pursuing:
		//If the guard has lost sight of the possessed NPC
		if (player->GetPossessing() == nullptr || player->GetIsAlive() == false || DistanceFrom(player->GetPossessing()).distance > SAFE_DISTANCE)
		{
			currentAction = GuardStates::ECharacterState::Patrolling;
			wanderingTicks = 0;
			System::Diagnostics::Debug::WriteLine("Guard switching from Pursuing to Patrolling");
		}//End of switch to Patrolling if

		//If the possessed NPC is within the range of engagement
		if ((player->GetIsAlive() == true && player->GetPossessing() != nullptr) && DistanceFrom(player->GetPossessing()).distance < ENGAGE_DISTANCE)
		{
			currentAction = GuardStates::ECharacterState::Engaging;
			System::Diagnostics::Debug::WriteLine("Guard switching from Pursuing to Engaging");
		}//End of switch to Engaging if

		//If the guard was clicked
		if(wasClicked)
		{
			isPossessed = true;
			wasClicked = false;
			currentAction = GuardStates::ECharacterState::Possessed;
			System::Diagnostics::Debug::WriteLine("Guard switching from Pursuing to Possessed");
		}//End of switch to Possessed if

		//Is the character standing on a non-solid block
		if(StandingOnAir())
		{
			currentAction = GuardStates::ECharacterState::Falling;
			landed = false;
			System::Diagnostics::Debug::WriteLine("Guard switching from Pursuing to Falling");
		}//End of switch to Falling if

		//Is the character marked as dead
		if(!isAlive)
		{
			currentAction = GuardStates::ECharacterState::Dead;
			System::Diagnostics::Debug::WriteLine("Guard switching from Pursuing to Dead");
		}//End of switch to Dead if
		break;//End of Pursuing case

	case GuardStates::ECharacterState::Engaging:

		//If the guard has lost sight of the possessed NPC OR  it is no longer possessed or it is dead
		if (player->GetPossessing() == nullptr || player->GetIsAlive() == false || DistanceFrom(player->GetPossessing()).distance > SAFE_DISTANCE)
		{
			currentAction = GuardStates::ECharacterState::Patrolling;
			wanderingTicks = 0;
			System::Diagnostics::Debug::WriteLine("Guard switching from Engaging to Patrolling");
		}//End of switch to Patrolling if

		//If the possessed NPC is outside of the range of engagement
		if (player->GetPossessing() != nullptr && DistanceFrom(player).distance > ENGAGE_DISTANCE)
		{
			currentAction = GuardStates::ECharacterState::Pursuing;
			System::Diagnostics::Debug::WriteLine("Guard switching from Engaging to Pursuing");
		}//End of switch to Pursuing if

		//If the guard was clicked
		if(wasClicked)
		{
			isPossessed = true;
			wasClicked = false;
			currentAction = GuardStates::ECharacterState::Possessed;
			System::Diagnostics::Debug::WriteLine("Guard switching from Engaging to Possessed");
		}//End of switch to Possessed if

		//Is the character standing on a non-solid block
		if(StandingOnAir())
		{
			currentAction = GuardStates::ECharacterState::Falling;
			landed = false;
			System::Diagnostics::Debug::WriteLine("Guard switching from Engaging to Falling");
		}//End of switch to Falling if

		//Is the character marked as dead
		if(!isAlive)
		{
			currentAction = GuardStates::ECharacterState::Dead;
			System::Diagnostics::Debug::WriteLine("Guard switching from Engaging to Dead");
		}//End of switch to Dead if
		break;//End of Engaging case

	case GuardStates::ECharacterState::Possessed:
		//If the player was clicked
		if(wasClicked)
		{
			isPossessed = false;
			wasClicked = false;
			currentAction = GuardStates::ECharacterState::Patrolling;
			System::Diagnostics::Debug::WriteLine("Guard switching from Possessed to Patrolling");
		}//End of switch to Patrolling if

		//Is the character standing on a non-solid block
		if(StandingOnAir())
		{
			currentAction = GuardStates::ECharacterState::Falling;
			landed = false;
			System::Diagnostics::Debug::WriteLine("Guard switching from Possessed to Falling");
		}//End of switch to Falling if

		//Has the space bar been pressed
		if (keys->Get((int)Keys::Space))
		{
			landed = false;
			//Make the Guard move upwards
			velocity.Y = GUARD_JUMP_STRENGTH;
			currentAction = GuardStates::ECharacterState::Falling;
			System::Diagnostics::Debug::WriteLine("Guard switching from Possessed to Jumping");
		}//End of switch to Falling upwards if

		//Is the character marked as dead
		if(!isAlive)
		{
			currentAction = GuardStates::ECharacterState::Dead;
			System::Diagnostics::Debug::WriteLine("Guard switching from Possessed to Dead");
		}//End of switch to Dead if
		break;//End of Possessed case

	case GuardStates::ECharacterState::Falling:
		//Has the Guard landed
		if (landed)
		{
			//If the Guard was possessed then set him back to the possessed state
			if (isPossessed)
			{
				currentAction = GuardStates::ECharacterState::Possessed;
				System::Diagnostics::Debug::WriteLine("Guard switching from Falling to Possessed");
			}
			else//Set him back to the patrolling state
			{
				currentAction = GuardStates::ECharacterState::Patrolling;
				System::Diagnostics::Debug::WriteLine("Guard switching from Falling to Patrolling");
			}
		}//End of switch to Patrolling or Possessed if
		break;//End of Falling case
	case GuardStates::ECharacterState::Dead:
		//Do nothing, it's dead Jim
		break;//End of Dead case
	}//End of switch statement
}//End of UpdateState(player,keys)

void Guard::PerformActions(AidenCharacter^ player, BitArray^ keys){
	//Performs specific actions based on the current state

	//Increase the number of ticks since the last interaction
	interactionTicks++;

	//Switch on the characters current action
	Rectangle temp = hitbox;
	switch ((GuardStates::ECharacterState)currentAction)
	{
	case GuardStates::ECharacterState::Patrolling:
		WanderPatrol();
		break;

	case GuardStates::ECharacterState::Paused:
		WorkPaused();
		break;

	case GuardStates::ECharacterState::Pursuing:
		Pursue(player);
		break;

	case GuardStates::ECharacterState::Engaging:
		Engage();
		break;

	case GuardStates::ECharacterState::Possessed:
		PossessedActions(keys);
		break;

	case GuardStates::ECharacterState::Falling:
		Fall(keys);
		break;

	case GuardStates::ECharacterState::Dead:
		//Adjust the character to be in the right place
		if (!deathAdjustment)
		{
			OnDeath();
		}
		//Do nothing, it's dead Jim
		break;
	}//End of switch statement
}//End of PerformActions(player,keys)

void Guard::Pursue(AidenCharacter^ player)
{
	//Move in the direction of the player
	Distance d = DistanceFrom(player);
	currentDirection = d.direction;
	
	Move();
}//End Pursue(player)

void Guard::Engage()
{
	//Fire a shot in the current direction
	
	//Calculate the bullets starting location
	int startX = location.X;
	int startY = location.Y;

	//If the Guard is possessed then adjust the shot's Y origin
	if (isPossessed)
	{
		startY += POSSESSED_SHOT_ORIGIN_Y; 
	}
	else//Use the default Y origin
	{
		startY += SHOT_ORIGIN_Y; 
	}
	eDirection shotDir;
	//Adjust the starting X based on direction;
	if (currentDirection == eDirection::Left)
	{
		//If the Guard is possessed then adjust the shots X origin
		if (isPossessed)
		{
			startX += POSSESSED_SHOT_ORIGIN_X;
		}
		else //Use the default x origin
		{
			startX += SHOT_ORIGIN_X;
		}
		shotDir = eDirection::Left;
	}//End of left if
	else //The Guard is moving right
	{
		//If the Guard is possessed then adjust the shots X origin
		if (isPossessed)
		{
			startX += GetFrameSize().Width - POSSESSED_SHOT_ORIGIN_X;
		}
		else//Use the default X origin
		{
			startX += GetFrameSize().Width - SHOT_ORIGIN_X;
		}
		shotDir = eDirection::Right;
	}//End of right else

	Point startLocation = Point(startX, startY);

	//Choose a random starting velocity
	int xVel = random->Next(BULLET_MIN_SPEED,BULLET_MAX_SPEED);
	Point startVelocity = Point(xVel, 0);

	//Tell the list of projectiles to add a new bullet
	bullets->Add(startLocation, startVelocity, shotDir);
}//End Engage()

void Guard::PossessedActions(BitArray^ keys)
{
	//Check if the player is shooting
	if (keys->Get((int)Keys::ShiftKey))
	{
		//Fire a bullet
		Engage();
	}
	
	//Do the other default possessed actions
	FiniteStateCharacter::PossessedActions(keys);
}//End PossessedActions(keys)
