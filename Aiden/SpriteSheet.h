#pragma once

	using namespace System;
	using namespace System::ComponentModel;
	using namespace System::Collections;
	using namespace System::Windows::Forms;
	using namespace System::Data;
	using namespace System::Drawing;

/*
	Author: Kierran McPherson
	Date: 18/11/2013
	Purpose: This class defines a SpriteSheet. Which is a single Bitmap image that contains multiple frames to create an animation.
	Known Bugs:
*/
ref class SpriteSheet
{
private:
	Bitmap^ sheet;
	int currentFrame;
	int nFrames;
	Size frameSize;
public:
	SpriteSheet(String^ filename, int startNFrames, int frameWidth, int frameHeight);
	
	
	void UpdateCurrentFrame();
	void ResetCurrentFrame();

	Bitmap^ GetFrame();

	//Sets and Gets
	int GetCurrentFrame()	{return currentFrame;}
	void SetCurrentFrame(int frameNum)	{currentFrame = frameNum;}

	int GetNFrames()	{return nFrames;}
	Size GetFrameSize()	{return frameSize;}
};

