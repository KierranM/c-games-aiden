#pragma once
#include "TileMap.h"
#include "ObjectMap.h"
#include "FSMCharacterList.h"
#include "Civilian.h"
#include "Guard.h"
#include "SpecialGuard.h"

#define X_LOCATION 1
#define Y_LOCATION 2

#define TILE_IMAGE_STRING 0
#define TILE_IS_LETHAL 1
#define TILE_IS_SOLID 2
#define TILE_IS_AIDEN_BARRIER 3
#define TILE_TRANSPARENT 4

#define CIVILIAN_STATES 6

#define SCIENTIST_FRAMES 1
#define SCIENTIST_HEIGHT 96
#define SCIENTIST_WIDTH 32
#define SCIENTIST_WALK_SPEED 4
#define SCIENTIST_DEADZONE 2
#define SCIENTIST_WANDERING_TICKS 200
#define SCIENTIST_WORKING_TICKS 100

#define MECHANIC_FRAMES 1
#define MECHANIC_HEIGHT 96
#define MECHANIC_WIDTH 32
#define MECHANIC_WALK_SPEED 4
#define MECHANIC_DEADZONE 2
#define MECHANIC_WANDERING_TICKS 100
#define MECHANIC_WORKING_TICKS 300

#define GUARD_STATES 7
#define GUARD_FRAMES 1
#define GUARD_HEIGHT 96
#define GUARD_WIDTH 64
#define GUARD_DEADZONE_X 10
#define GUARD_DEADZONE_Y 2
#define GUARD_WANDERING_TICKS 300
#define GUARD_PAUSED_TICKS 100

#define SPECIAL_GUARD_STATES 5
#define SPECIAL_GUARD_FRAMES 1
#define SPECIAL_GUARD_HEIGHT 96
#define SPECIAL_GUARD_WIDTH 64
#define SPECIAL_GUARD_DEADZONE_X 10
#define SPECIAL_GUARD_DEADZONE_Y 2
#define SPECIAL_GUARD_WANDERING_TICKS 300
#define SPECIAL_GUARD_PAUSED_TICKS 100

using namespace System;
using namespace System::ComponentModel;
using namespace System::Collections;
using namespace System::Windows::Forms;
using namespace System::Data;
using namespace System::Drawing;


//Namespace prevents scoping issues
namespace NPCTypes{
	public enum eCharacterTypes{
		Scientist,
		Maintenance,
		Guard,
		Special_Guard
	};
};

/*
	Author: Kierran McPherson
	Date: 18/11/2013
	Purpose: This class defines a level in the game. On creation it is provided the name of a folder containing
			 files with all the information required to set up the level. This makes it very easy to create new levels
			 for the game.
	Known Bugs:
*/

ref class Level
{
private:
	String^ levelName;
	String^ folderPath;
	TileMap^ map;
	ObjectMap^ objectMap;
	ProjectileList^ bulletList;
	Bitmap^ background;
	Graphics^ canvas;
	Point playerStart;
	FSMCharacterList^ levelNPCS;
	Random^ rand;
	Rectangle world;
	int parTime;
public:
	Level(String^ startFolderPath, Graphics^ startCanvas, Random^ startRand);

	void OnClick(Point p);//Debug only
	void SpawnScientist(Point startLocation);
	void SpawnMechanic(Point startLocation);
	void SpawnGuard(Point startLocation);
	void SpawnSpecialGuard(Point startLocation);

	//Gets and Sets
	String^ GetLevelName()	{return levelName;}
	void SetLevelName(String^ newLevelName)	{levelName = newLevelName;}

	TileMap^ GetTileMap()	{return map;}
	ObjectMap^ GetObjectMap()	{return objectMap;}
	Bitmap^ GetBackGround()	{return background;}
	int GetMapWidth()	{return map->GetWidth();}
	int GetMapHeight()	{return map->GetHeight();}
	int GetParTime()	{return parTime;}
	Point GetPlayerStartLocation() {return playerStart;}
	FSMCharacterList^ GetLevelNPCS()	{return levelNPCS;}
	ProjectileList^ GetLevelProjectileList()	{return bulletList;}
	Rectangle GetWorldRectangle()	{return world;}
};

