#pragma once
#include "Character.h"

#define NUM_DIRECTIONS 3
#define UPWARDS_SHIFT 20

public enum eAidenActions{
	Walk,
	Stop
};

//In a namespace to prevent scoping issues
namespace AidenDirections
{
	public enum eAidenDirections
	{
		Left,
		None,
		Right
	};
};

/*
	Author: Kierran McPherson
	Date: 18/11/2013
	Purpose: This class defines the character Aiden. It is a special character that moves through most types of tile,
			 and does not get affected by gravity
	Known Bugs:
*/

ref class AidenCharacter :
public Character
{

private:
	AidenDirections::eAidenDirections currentDirection;
	Character^ possessing;
	bool isVisible;
public:
	AidenCharacter(
		array<String^, TWO>^ filenames, 
		array<int,TWO>^ frameNumbers, 
		array<Size>^ frameSize,
		Point startLocation,
		int startAction,
		int startXDeadZone,
		int startYDeadZone,
		Graphics^ startCanvas,
		Random^ startRandom,
		Rectangle startBounds,
		TileMap^ startMap,
		ObjectMap^ startObjects
		);

	void CenterOn(Character^ otherGuy);
	virtual void Move()	override;
	virtual void Update() override;
	virtual void Draw(Point viewportWorldLocation) override;
	virtual bool CheckTileX(Rectangle toCheck) override;
	virtual bool CheckTileY(Rectangle toCheck) override;
	bool IsGameOver();
	bool CheckForWin();
	
	//Gets and Sets
	virtual Point GetCenter() override;

	AidenDirections::eAidenDirections GetCurrentDirection()	{return currentDirection;}
	void SetCurrentDirection(AidenDirections::eAidenDirections newDir)	{currentDirection = newDir;}
	
	Character^ GetPossessing()	{return possessing;}
	void SetPossessing(Character^ possess)	{possessing = possess;}
};

