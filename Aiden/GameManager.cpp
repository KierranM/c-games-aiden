#include "stdafx.h"
#include "GameManager.h"

GameManager::GameManager(Graphics^ startPanelGraphics, Size startClientSize, array<String^>^ startLevelNames ,array<Label^>^ startLabels)
{
	//Create the game manager using the given and default values
	
	//Store the given values
	clientSize = startClientSize;
	panelGraphics = startPanelGraphics;
	labels = startLabels;
	levelNames = startLevelNames;

	//Create the buffer
	offScreen = gcnew Bitmap(clientSize.Width, clientSize.Height);
	buffer = Graphics::FromImage(offScreen);
	buffer->SmoothingMode = Drawing2D::SmoothingMode::AntiAlias;
	buffer->InterpolationMode = Drawing2D::InterpolationMode::HighQualityBicubic;

	//Set up the default values
	gameFont = gcnew Font("High Tower Text", FONT_SIZE);
	rand = gcnew Random();
	keys = gcnew BitArray(NUMBER_OF_KEYS);
	npcs = gcnew FSMCharacterList();
	bgOne = Rectangle(0,0,clientSize.Width,clientSize.Height);
	bgTwo = Rectangle(clientSize.Width,0,clientSize.Width,clientSize.Height);
	gameover = false;
	hasWon = false;
	startScore = START_SCORE;
	currentLevelNumber = 0;
	StartLevel();
}//End constructor 2 params

void GameManager::OnTick()
{
	//Performs the game cycle. Called from within the forms timer tick
	DoKeyEvents();
	if (!hasWon)
	{
		
		//If the game is not over then move Aiden
		if (!gameover)
		{
			DateTime current = DateTime::Now;
			TimeSpan timeTaken = current.Subtract(started);
			int mins = timeTaken.Seconds/SECONDS_IN_MINUTE;
			int secs = timeTaken.Seconds%SECONDS_IN_MINUTE;
		
			labels[ELabelArrayLocations::CurrentTime]->Text = "Current: " + timeTaken.ToString("m':'ss'.'ff");
			aiden->Move();
			aiden->Update();
		
			//Check if the game is over
			gameover = aiden->IsGameOver();
			hasWon = aiden->CheckForWin();
		}

		//Update the other game entities
		viewport->CenterOn(aiden);
		npcs->UpdateStates(aiden,keys);
		npcs->PerformActions(aiden,keys, projectiles); 
		projectiles->UpdateAll();

		//Draw the main visuals
		DrawBackground();
		viewport->Draw();

		//Draw the other entities
		npcs->DrawAll(viewport->GetWorldLocation(), viewport->GetSizeInPixels());
		aiden->Draw(viewport->GetWorldLocation());
		projectiles->DrawAll(viewport->GetWorldLocation(), viewport->GetSizeInPixels());

		//If the game is over draw a string to the screen
		if(gameover)
		{
			labels[ELabelArrayLocations::Feedback]->Text = "GAME OVER\n\tYour Host Died";
		}
		else //The game is not over
		{
			//Has the user won the level
			if (hasWon)
			{
				CalculateScore();
				labels[ELabelArrayLocations::Feedback]->Text = "Level Complete!";
			}//End of haswon if
		}//End of else
	}//End of !haswon if
	else //The player won
	{
		buffer->Clear(Color::Black);
		buffer->DrawString("Final Score for level " + currentLevel->GetLevelName() + " is " + startScore,gameFont,Brushes::White, SCORE_SUMMARY_POSITION_X, SCORE_SUMMARY_POSITION_Y);
		if (startScore > endScore)
		{
			startScore -= SCORE_DECREMENT;
		}
		else //It has finished counting down
		{
			int y = SCORE_SUMMARY_POSITION_Y + FONT_SIZE*TWO;
			String^ deaths;
			if(npcs->CountDead() > 0)
			{
				deaths = npcs->CountDead() + " deaths";
			}
			else
			{
				deaths = "No Deaths";
			}

			buffer->DrawString(deaths,gameFont,Brushes::White, SCORE_SUMMARY_POSITION_X, y);
			y += FONT_SIZE;
			DateTime current = DateTime::Now;
			TimeSpan timeTaken = current.Subtract(started);
			int secondsTaken = timeTaken.Seconds;
			int parSeconds = currentLevel->GetParTime();
	
			String^ time;
			if (secondsTaken > parSeconds)
			{
				time = (secondsTaken - parSeconds) + " seconds over par";
			}
			else
			{
				time = "Completed under par time";
			}
			buffer->DrawString(time,gameFont,Brushes::White, SCORE_SUMMARY_POSITION_X, y);

			y+=FONT_SIZE*TWO;
			
			//Increase the current level number
			currentLevelNumber++;
			//Display the prompt to continue to the next level
			String^ nextLevelMessage;
			if (currentLevelNumber < levelNames->Length)
			{
				nextLevelMessage = "Press enter to go to the next level";
			}
			else
			{
				nextLevelMessage = "Thanks for playing more levels coming soon";
			}

			buffer->DrawString(nextLevelMessage,gameFont,Brushes::White, SCORE_SUMMARY_POSITION_X, y);

		}//End finished counting else
	}//End haswon else
	//Draw the buffer to the panel
	panelGraphics->DrawImage(offScreen,0,0,clientSize.Width, clientSize.Height);
}//End OnTick()

void GameManager::OnMouseClick(MouseEventArgs^ e)
{
	//Occurs when the mouse is clicked on the panel

	//If the button used to click was the left button
	if (e->Button == MouseButtons::Left)
	{
		//Check if aiden is currently possessing someone
		if(aiden->GetPossessing() != nullptr)
		{
			//End the possession
			FiniteStateCharacter^ possessed = (FiniteStateCharacter^)aiden->GetPossessing();
			possessed->SetWasClicked(true);
			aiden->CenterOn(possessed);
			aiden->SetPossessing(nullptr);
		}
		else//Aiden is not currently possessing someone
		{
			//Check if an NPC was clicked
			
			//Calculate the clicked position relative to the world
			Point clicked = e->Location;
			Point viewportLocation = viewport->GetWorldLocation();
			int clickedWorldX = clicked.X + viewportLocation.X;
			int clickedWorldY = clicked.Y + viewportLocation.Y;
			Point clickedWorldLocation = Point(clickedWorldX,clickedWorldY);
			//Write the clicked point out to the debug console
			System::Diagnostics::Debug::WriteLine("User clicked at [" + clickedWorldLocation.X + "," + clickedWorldLocation.Y + "]");

			//Get the clicked NPC
			FiniteStateCharacter^ clickedNPC = npcs->GetClickedNPC(clickedWorldLocation);
			if (clickedNPC != nullptr)
			{
				//Check if the clicked NPC was close enough
				if (clickedNPC->DistanceFrom(aiden).distance <= POSSESS_DISTANCE)
				{
					clickedNPC->SetWasClicked(true);
					aiden->SetPossessing(clickedNPC);
				}//End click distance if
			}//End clickedNPC nullptr if
		}//End not possessing else
	}//End left mouse button if
}//End OnMouseClick(e)

void GameManager::DoKeyEvents()
{
	//Checks which keys are pressed and performs the appropriate action

	//If the game is has been won
	if (hasWon)
	{
		//And there are more levels to go
		if (currentLevelNumber < levelNames->Length)
		{
			//And the enter key is pressed
			if (keys->Get((int)Keys::Enter))
			{
				//Start the new level
				StartLevel();
			}//End enter key if
		}//End level number if
	}//End haswon if

	//The player wants to exit
	if(keys->Get((int)Keys::Escape))
	{
		Application::Exit();
	}

	//Set aidens direction to none for a start
	aiden->SetCurrentDirection(AidenDirections::eAidenDirections::None);

	//Start off with a zero velocity
	Point aidenVel = Point(0,0);

	//If the A key is pressed
	if (keys->Get((int)Keys::A))
	{
		//Move left
		aidenVel.X = -AIDEN_SPEED;
		aiden->SetCurrentDirection(AidenDirections::eAidenDirections::Left);
	}
	
	//The D key is pressed
	if (keys->Get((int)Keys::D))
	{
		//Move Right
		aidenVel.X = AIDEN_SPEED;
		aiden->SetCurrentDirection(AidenDirections::eAidenDirections::Right);
	}

	//The W key is pressed
	if (keys->Get((int)Keys::W))
	{
		//Move up
		aidenVel.Y = -AIDEN_SPEED;
	}
	
	//The S key is pressed
	if (keys->Get((int)Keys::S))
	{
		//Move down
		aidenVel.Y = AIDEN_SPEED;
	}

	//Set aidens velocity
	aiden->SetVelocity(aidenVel);
}//End DoKeyEvents()

void GameManager::DrawBackground()
{
	//Draws the background in a tiling manner

	//Get the viewports current location
	Point vpWorldLoc = viewport->GetWorldLocation();

	//Calculate how much to move
	int movement = -(vpWorldLoc.X) * PARALLAX_FACTOR;

	//Move the background rectangles
	bgOne.X = movement;
	bgTwo.X = movement + bgOne.Width - BACKGROUND_CORRECTION;

	//Wrap the first rectangle around
	if(bgOne.Right <0)
	{
		bgOne.X = clientSize.Width;
	}
	//Wrap the second rectangle around
	if (bgTwo.Right < 0)
	{
		bgTwo.X = clientSize.Width;
	}

	//Draw the backgrounds
	buffer->DrawImage(currentLevel->GetBackGround(),bgOne);
	buffer->DrawImage(currentLevel->GetBackGround(), bgTwo);
}//End of DrawBackground()

void GameManager::CalculateScore()
{
	//Calculates the end score of the player
	endScore = START_SCORE;

	//Count the number of dead
	int numberDead = npcs->CountDead();

	endScore -= numberDead * NPC_DEATH_SCORE_PENALTY;

	DateTime current = DateTime::Now;
	TimeSpan timeTaken = current.Subtract(started);
	int secondsTaken = timeTaken.Seconds;
	int parSeconds = currentLevel->GetParTime();
	
	if (secondsTaken > parSeconds)
	{
		int subtraction = (secondsTaken - parSeconds) * SECOND_OVER_SCORE_PENALTY;
		endScore -= subtraction;
	}
}//End CalculateScore()

void GameManager::StartLevel()
{
	//Load the level
	currentLevel = gcnew Level(levelNames[currentLevelNumber], buffer, rand);
	labels[ELabelArrayLocations::LevelName]->Text = currentLevel->GetLevelName();
	//Show the Par Time
	int pt = currentLevel->GetParTime();
	int parMins = pt/SECONDS_IN_MINUTE;
	int parSecs = pt%SECONDS_IN_MINUTE;
	labels[ELabelArrayLocations::ParTime]->Text = "Par: " + parMins + ":" + parSecs;
	labels[ELabelArrayLocations::CurrentTime]->Text = "Current: 0:00";
	//Get the world rectangle from the current level
	world = currentLevel->GetWorldRectangle();
	//Get the lists from the current level
	npcs = currentLevel->GetLevelNPCS();
	projectiles = currentLevel->GetLevelProjectileList();

	//Set up the viewport
	Size vpSize = Size(VIEWPORT_WIDTH, VIEWPORT_HEIGHT);
	viewport = gcnew ViewPort(vpSize, Point(0,0), currentLevel->GetTileMap(), currentLevel->GetObjectMap(), buffer);

	//Set up Aiden
	array<String^,TWO>^ aidenFilenames = gcnew array<String^,TWO>(AIDEN_ACTIONS, NUM_DIRECTIONS);

	aidenFilenames[0,AidenDirections::eAidenDirections::Left] = "./Characters/Aiden/move-left.png";
	aidenFilenames[0,AidenDirections::eAidenDirections::None] = "./Characters/Aiden/idle-left.png";
	aidenFilenames[0,AidenDirections::eAidenDirections::Right] = "./Characters/Aiden/move-right.png";

	array<int,TWO>^ aidenFrameNumbers = gcnew array<int,TWO>{{AIDEN_FRAMES,AIDEN_FRAMES,AIDEN_FRAMES}};
	array<Size>^ aidenFrameSizes = gcnew array<Size>{Size(AIDEN_WIDTH,AIDEN_HEIGHT)};

	//Create Aiden
	aiden = gcnew AidenCharacter(aidenFilenames, aidenFrameNumbers, aidenFrameSizes, currentLevel->GetPlayerStartLocation(), 0, AIDEN_DEADZONE, AIDEN_DEADZONE,buffer, rand,world, currentLevel->GetTileMap(), currentLevel->GetObjectMap());
	started = DateTime::Now;
}//End StartLevel()