#pragma once
#include "Character.h"
#include "FiniteStateCharacter.h"
#include "AidenCharacter.h"

#define VIEW_DISTANCE 300
#define SAFE_DISTANCE 500
#define ENGAGE_DISTANCE 200
#define EYE_LEVEL 15
#define JUMP_STRENGTH -17
#define GRAVITY 1;
#define NO_DIRECTION_JUMP -1
#define INTERACTION_TIME 10

#define FSM_DIRECTIONS 2

//This struct is used for returning both a distance and a direction in the DistanceFrom method
struct Distance
{
	int distance;
	eDirection direction;
};

/*
	Author: Kierran McPherson
	Date: 18/11/2013
	Purpose: The FiniteStateCharacter is a child of Character that is controlled by an FSM. 
			 The decendents of this class implement different states, events, and actions depending
			 on what they require
	Known Bugs:
*/

ref class FiniteStateCharacter :
public Character
{
protected:
	bool canBePossessed;
	bool isPossessed;
	bool landed;
	eDirection currentDirection;
	int wanderingTime;
	int wanderingTicks;
	int workingTime;
	int workingTicks;
	int interactionTicks;
	bool wasClicked;
	bool deathAdjustment;
public:
	FiniteStateCharacter^ Next; //LinkedList compatibility

public:
	FiniteStateCharacter();
	FiniteStateCharacter(		
		array<String^, TWO>^ filenames, 
		array<int,TWO>^ frameNumbers, 
		array<Size>^ frameSize,
		Point startLocation,
		int startAction,
		int startXDeadZone,
		int startYDeadZone,
		Graphics^ startCanvas,
		Random^ startRandom,
		Rectangle startBounds,
		TileMap^ startMap,
		ObjectMap^ startObjects,
		bool startCanBePossessed,
		int startWanderingTime,
		int startWanderingTicks,
		int startWorkingTime,
		int startWorkingTicks
		);

	
	bool StandingOnAir();
	bool PossessedNPCInLOS(AidenCharacter^ player, int viewDistance, bool requireEyeLevel);
	bool Contains(Point p);	
	Distance DistanceFrom(Character^ character);
	void MoveToTopOfTile();
	virtual void UpdateState(AidenCharacter^ player, BitArray^ keys);
	virtual void PerformActions(AidenCharacter^ player, BitArray^ keys);
	virtual void Move() override;
	virtual void Draw(Point viewportWorldLocation) override;

	//Actions
	void WanderPatrol();
	void WorkPaused();
	virtual void PossessedActions(BitArray^ keys);
	void Fall(BitArray^ keys);
	void OnDeath();

	//Gets and Sets
	bool GetIsPossessed()	{return isPossessed;}
	void SetIsPossessed(bool possessed)	{isPossessed = possessed;}

	void SetWasClicked(bool clicked)	{wasClicked = clicked;}

	virtual Size GetFrameSize()	override {return spriteSheets[currentAction,currentDirection]->GetFrameSize();}
};

