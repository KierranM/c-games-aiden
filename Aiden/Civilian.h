#pragma once
#include "FiniteStateCharacter.h"

#define WORK_CHANCE 20
#define CIVILIAN_JUMP_STRENGTH -18
#define CIVILIAN_MOVE_SPEED 6

//Inside a namespace to prevent scoping issues
namespace CivilianStates
{
	private enum ECharacterState
	{
		Wandering,
		Working,
		Fleeing,
		Possessed,
		Falling,
		Dead
	};
};

/*
	Author: Kierran McPherson
	Date: 18/11/2013
	Purpose: This class defines the Civilian which is a type of FiniteStateCharacter. It wanders about the world
			 and can be possessed
	Known Bugs:
*/

ref class Civilian :
public FiniteStateCharacter
{
public:
	Civilian(
		array<String^, TWO>^ filenames, 
		array<int,TWO>^ frameNumbers, 
		array<Size>^ frameSize,
		Point startLocation,
		int startAction,
		int startXDeadZone,
		int startYDeadZone,
		Graphics^ startCanvas,
		Random^ startRandom,
		Rectangle startBounds,
		TileMap^ startMap,
		ObjectMap^ startObjects,
		int startWanderingTime,
		int startWanderingTicks,
		int startWorkingTime,
		int startWorkingTicks
	);

	virtual void UpdateState(AidenCharacter^ player, BitArray^ keys) override;
	virtual void PerformActions(AidenCharacter^ player, BitArray^ keys) override;

	void Flee(AidenCharacter^ player);
};