#include "stdafx.h"
#include "FSMCharacterList.h"



FSMCharacterList::FSMCharacterList(void)
{
	//Make both pointers point to nothing
	head = nullptr;
	tail = nullptr;
}//End constructor no params


FSMCharacterList::FSMCharacterList(FiniteStateCharacter^ first){
	//Set both pointers to point to the first pellet
	head = first;
	tail = first;
}//End constructor 1 param


int FSMCharacterList::Count(){
	//Loops through the list and counts the number of characters
	int count = 0;
	FiniteStateCharacter^ nodeWalker = head;
	while(nodeWalker != nullptr){
		nodeWalker = nodeWalker->Next;
		count++;
	}

	return count;
}//End Count()

int FSMCharacterList::CountDead(){
	//Loops through the list and counts the number of dead characters
	int count = 0;
	FiniteStateCharacter^ nodeWalker = head;
	while(nodeWalker != nullptr){
		if (nodeWalker->GetIsAlive() == false)
		{
			count++;
		}
		nodeWalker = nodeWalker->Next;
	}

	return count;
}//End CountDead()


void FSMCharacterList::Add(FiniteStateCharacter ^node){
	if(tail == nullptr){ //Empty list
		head = node;
		tail = node;
	}
	else{ //Adding to end
		tail->Next = node;
		tail = node;
	}
}//End Add(node)


void FSMCharacterList::Remove(FiniteStateCharacter ^node){
	FiniteStateCharacter^ nodeWalker;

	//start at the beginning of the list
	nodeWalker = head;

	//If the node to be deleted is the head
	if(node == head){
		//If the node is the head and has no next
		if(node->Next == nullptr){
			head = nullptr;
			tail = nullptr;
		}
		else{ //Node is the first but not the only item in the list
			head = head->Next;
		}
	}
	else{ //The node to be deleted are not head
		FiniteStateCharacter^ temp;
		while(nodeWalker->Next != node){
			nodeWalker = nodeWalker->Next;
		}
		//Store the node
		temp = nodeWalker->Next;
		//Check if the node that is being removed is the tail
		if(temp == tail){
			//Make the tail the current node and set its next to nothing
			tail = nodeWalker;
			nodeWalker->Next = nullptr;
		}
		else{
			//Otherwise make the list skip over the node that is being removed
			nodeWalker->Next = temp->Next;
		}//End not tail else
	}//End not head else
}//End Remove(node)


void FSMCharacterList::DrawAll(Point viewportWorldPosition, Size viewportSize){
	//Loops through all characters in the list and tells them to draw themselves if they are within the viewport area
	FiniteStateCharacter^ nodeWalker = head;

	//If the list is not empty
	if(nodeWalker != nullptr){

		while(nodeWalker != nullptr){

			//Get the characters location relevant to the viewport
			Point characterViewportPosition = nodeWalker->CalculateViewPortLocation(viewportWorldPosition);

			//Get the size of the characters current frame
			Size characterSize = nodeWalker->GetFrameSize();

			//If the character is within the viewport
			if (InViewport(characterViewportPosition, characterSize, viewportSize))
			{
				nodeWalker->Draw(viewportWorldPosition);
			}

			nodeWalker = nodeWalker->Next;
		}//End while
	}//End empty list if
}//End DrawAll(viewportWorldPosition, viewportSize)

bool FSMCharacterList::InViewport(Point characterLocation, Size characterSize, Size viewportSize)
{
	//Checks if the given coordinates are within the bounds of the viewport

	//Left side check
	if ((characterLocation.X + characterSize.Width) >= 0)
	{
		//Right side check
		if (characterLocation.X <= (viewportSize.Width))
		{
			//Top side check
			if ((characterLocation.Y + characterSize.Height) >= 0)
			{
				//Bottom side check
				if (characterLocation.Y <= (viewportSize.Height))
				{
					//The coordinate is within the bounds of the viewport
					return true;
				}//End bottom side if
			}//End top side if
		}//End right side if
	}//End left side if

	//The coordinate is not in the bounds of the viewport
	return false;
}//End InViewport(characterLocation,characterSize,viewportSize)


void FSMCharacterList::UpdateStates(AidenCharacter^ player, BitArray^ keys){
	//Loops through all characters in the list and updates their states
	FiniteStateCharacter^ nodeWalker = head;

	//If the list is not empty
	if(nodeWalker != nullptr){
		//Loop until the end of the list
		while(nodeWalker != nullptr){
			nodeWalker->UpdateState(player,keys);
			nodeWalker = nodeWalker->Next;
		}//End while
	}//End empty list if
}//End UpdateStates(player,keys)

void FSMCharacterList::PerformActions(AidenCharacter^ player, BitArray^ keys, ProjectileList^ bullets){
	//Loops through all characters in the list and performs their actions, and checks for collisions with projectiles
	FiniteStateCharacter^ nodeWalker = head;

	//If the list is not empty
	if(nodeWalker != nullptr){
		//Loop until the end of the list
		while(nodeWalker != nullptr){
			bullets->CollisionsWith(nodeWalker);
			nodeWalker->PerformActions(player,keys);
			nodeWalker = nodeWalker->Next;
		}//End while
	}//End empty list if
}//End PerformActions(player,keys,bullets)

FiniteStateCharacter^ FSMCharacterList::GetClickedNPC(Point clickedWorldLocation)
{
	//Loops through all characters in the list checks if any of them were clicked
	FiniteStateCharacter^ clicked = nullptr;
	FiniteStateCharacter^ nodeWalker = head;
	//If the list is not empty
	if(nodeWalker != nullptr){
		//Loop until the end of the list
		while(nodeWalker != nullptr){
			//If the current node contains the clicked coordinate
			if (nodeWalker->Contains(clickedWorldLocation))
			{
				//Store the clicked NPC
				clicked = nodeWalker;
			}
			nodeWalker = nodeWalker->Next;
		}//End while
	}//End empty list if

	//Return the clicked character
	return clicked;
}//End GetClickedNPC(clickedWorldLocation)