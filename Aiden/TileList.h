#pragma once
#include "Tile.h"
	using namespace System;
	using namespace System::ComponentModel;
	using namespace System::Collections;
	using namespace System::Windows::Forms;
	using namespace System::Data;
	using namespace System::Drawing;

/*
	Author: Kierran McPherson
	Date: 18/11/2013
	Purpose: This class defines TileList which holds an array of tiles indexed by the tile type
	Known Bugs:
*/
ref class TileList
{
private:
	array<Tile^>^ tiles;
public:
	TileList(int numberOfTiles);

	//Gets and Sets

	Bitmap^ GetTile(int index)	{return tiles[index]->GetTileImage();}
	void SetTile(Tile^ tile, int index)	{tiles[index] = tile;}

	bool GetIsSolid(int index)	{return tiles[index]->GetIsSolid();}
	bool GetIsLethal(int index)	{return tiles[index]->GetIsLethal();}
	bool GetIsAidenBarrier(int index)	{return tiles[index]->GetIsAidenBarrier();}
};

