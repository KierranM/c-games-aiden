#include "stdafx.h"
#include "SpriteSheet.h"


SpriteSheet::SpriteSheet(String^ filename, int startNFrames, int frameWidth, int frameHeight)
{
	//Create a SpriteSheet with the given parameters
	sheet = gcnew Bitmap(filename);
	currentFrame = 0;
	nFrames = startNFrames;
	frameSize = Size(frameWidth, frameHeight);

	//Make the Spritesheets background transparent based off the colour of the top left pixel
	sheet->MakeTransparent(sheet->GetPixel(0,0));
}//End constructor 4 params

Bitmap^ SpriteSheet::GetFrame()
{
	//Get the current frame of the SpriteSheet

	//Create a bitmap the size of a single frame
	Bitmap^ frame = gcnew Bitmap(frameSize.Width, frameSize.Height);
	//Create a graphics object for that bitmap
	Graphics^ g = Graphics::FromImage(frame);

	//Where the frame should draw
	Rectangle destRect = Rectangle(0,0,frame->Width, frame->Height);
	//Where the frame is coming from out of the overall spritesheet
	Rectangle srcRect = Rectangle(currentFrame * frameSize.Width, 0, frameSize.Width, frameSize.Height);

	//Draw the frame to the bitmap
	g->DrawImage(sheet,destRect, srcRect, GraphicsUnit::Pixel);

	//Dispose of the Graphics object
	g->~Graphics();

	//Return the single frame
	return frame;
}//End GetFrame()

void SpriteSheet::UpdateCurrentFrame()
{
	//Update the SpriteSheets current frame number in a looping fashion
	currentFrame += 1;
	currentFrame = currentFrame % nFrames;
}//End UpdateCurrentFrame()

void SpriteSheet::ResetCurrentFrame()
{
	//Reset the frame counter
	currentFrame = 0;
}//End ResetCurrentFrame()
