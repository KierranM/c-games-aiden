#pragma once
#include "TileMap.h"
#include "ObjectMap.h"
#include "Character.h"

using namespace System::Drawing;

/*
	Author: Kierran McPherson
	Date: 18/11/2013
	Purpose: This class describes a projectile, which is a fast moving object capable of killing NPC's that
			 it comes into contact with.
	Known Bugs:
*/
ref class Projectile
{
private:
	Point location;
	Point velocity;
	TileMap^ map;
	ObjectMap^ objects;
	Bitmap^ image;
	Graphics^ canvas;
	eDirection horizontalDirection;
	Rectangle boundary;
	bool isAlive;
public:
	Projectile^ Next; //For LinkedList compatability
public:
	Projectile(
		Point startLocation,
		Point startVelocity,
		eDirection startDirection,
		Rectangle startBoundary,
		TileMap^ startMap,
		ObjectMap^ startObjects,
		Graphics^ startCanvas,
		String^ imageFileName
		);

	void Draw(Point viewportWorldLocation);
	void Move();
	bool CheckTile(Rectangle toCheck);
	bool OutOfBounds(Rectangle toCheck);
	bool CollidesWith(Character^ character);

	//Gets and Sets
	Size GetSize()	{return image->Size;}

	Point GetLocation()	{return location;}
	void SetLocation(Point newLoc)	{location = newLoc;}

	bool GetIsAlive()	{return isAlive;}
	void SetIsAlive(bool alive)	{isAlive = alive;}
};

