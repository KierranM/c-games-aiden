#pragma once
#include "GameManager.h"

#define PANEL_HEIGHT 768
#define PANEL_WIDTH 832
#define WINDOW_HEIGHT 768
#define WINDOW_WIDTH 1024

#define NUMBER_OF_LEVELS 1

namespace Aiden {

	using namespace System;
	using namespace System::ComponentModel;
	using namespace System::Collections;
	using namespace System::Windows::Forms;
	using namespace System::Data;
	using namespace System::Drawing;

	/// <summary>
	/// Summary for Form1
	/// </summary>
	public ref class Form1 : public System::Windows::Forms::Form
	{
	

	protected:
		/// <summary>
		/// Clean up any resources being used.
		/// </summary>
		~Form1()
		{
			if (components)
			{
				delete components;
			}
		}
	private: System::Windows::Forms::Timer^  gameTimer;
	private: System::Windows::Forms::Panel^  gamePanel;
	private: System::Windows::Forms::Label^  titleLabel;
	private: System::Windows::Forms::Label^  controlTitleLabel;
	private: System::Windows::Forms::Label^  controls;

	private: System::Windows::Forms::Label^  currentLevelName;
	private: System::Windows::Forms::Label^  timeTaken;
	private: System::Windows::Forms::Label^  feedback;
	private: System::Windows::Forms::Label^  parTime;



	protected: 

	protected: 
	private: System::ComponentModel::IContainer^  components;

	private:
		/// <summary>
		/// Required designer variable.
		/// </summary>


#pragma region Windows Form Designer generated code
		/// <summary>
		/// Required method for Designer support - do not modify
		/// the contents of this method with the code editor.
		/// </summary>
		void InitializeComponent(void)
		{
			this->components = (gcnew System::ComponentModel::Container());
			System::ComponentModel::ComponentResourceManager^  resources = (gcnew System::ComponentModel::ComponentResourceManager(Form1::typeid));
			this->gameTimer = (gcnew System::Windows::Forms::Timer(this->components));
			this->gamePanel = (gcnew System::Windows::Forms::Panel());
			this->titleLabel = (gcnew System::Windows::Forms::Label());
			this->controlTitleLabel = (gcnew System::Windows::Forms::Label());
			this->controls = (gcnew System::Windows::Forms::Label());
			this->currentLevelName = (gcnew System::Windows::Forms::Label());
			this->timeTaken = (gcnew System::Windows::Forms::Label());
			this->feedback = (gcnew System::Windows::Forms::Label());
			this->parTime = (gcnew System::Windows::Forms::Label());
			this->SuspendLayout();
			// 
			// gameTimer
			// 
			this->gameTimer->Interval = 50;
			this->gameTimer->Tick += gcnew System::EventHandler(this, &Form1::gameTimer_Tick);
			// 
			// gamePanel
			// 
			this->gamePanel->Location = System::Drawing::Point(0, 0);
			this->gamePanel->Name = L"gamePanel";
			this->gamePanel->Size = System::Drawing::Size(832, 768);
			this->gamePanel->TabIndex = 0;
			this->gamePanel->MouseClick += gcnew System::Windows::Forms::MouseEventHandler(this, &Form1::gamePanel_MouseClick);
			// 
			// titleLabel
			// 
			this->titleLabel->AutoSize = true;
			this->titleLabel->Font = (gcnew System::Drawing::Font(L"High Tower Text", 36, System::Drawing::FontStyle::Bold, System::Drawing::GraphicsUnit::Point, 
				static_cast<System::Byte>(0)));
			this->titleLabel->Location = System::Drawing::Point(846, 36);
			this->titleLabel->Name = L"titleLabel";
			this->titleLabel->Size = System::Drawing::Size(150, 57);
			this->titleLabel->TabIndex = 0;
			this->titleLabel->Text = L"Aiden";
			// 
			// controlTitleLabel
			// 
			this->controlTitleLabel->AutoSize = true;
			this->controlTitleLabel->BackColor = System::Drawing::Color::Transparent;
			this->controlTitleLabel->Font = (gcnew System::Drawing::Font(L"High Tower Text", 15.75F, System::Drawing::FontStyle::Regular, System::Drawing::GraphicsUnit::Point, 
				static_cast<System::Byte>(0)));
			this->controlTitleLabel->Location = System::Drawing::Point(828, 122);
			this->controlTitleLabel->Name = L"controlTitleLabel";
			this->controlTitleLabel->Size = System::Drawing::Size(95, 25);
			this->controlTitleLabel->TabIndex = 1;
			this->controlTitleLabel->Text = L"Controls:";
			// 
			// controls
			// 
			this->controls->AutoSize = true;
			this->controls->BackColor = System::Drawing::Color::Transparent;
			this->controls->Font = (gcnew System::Drawing::Font(L"High Tower Text", 12, System::Drawing::FontStyle::Regular, System::Drawing::GraphicsUnit::Point, 
				static_cast<System::Byte>(0)));
			this->controls->Location = System::Drawing::Point(829, 147);
			this->controls->Name = L"controls";
			this->controls->Size = System::Drawing::Size(184, 228);
			this->controls->TabIndex = 2;
			this->controls->Text = resources->GetString(L"controls.Text");
			// 
			// currentLevelName
			// 
			this->currentLevelName->AutoSize = true;
			this->currentLevelName->Font = (gcnew System::Drawing::Font(L"High Tower Text", 15.75F, System::Drawing::FontStyle::Regular, System::Drawing::GraphicsUnit::Point, 
				static_cast<System::Byte>(0)));
			this->currentLevelName->Location = System::Drawing::Point(904, 418);
			this->currentLevelName->Name = L"currentLevelName";
			this->currentLevelName->Size = System::Drawing::Size(0, 25);
			this->currentLevelName->TabIndex = 3;
			// 
			// timeTaken
			// 
			this->timeTaken->AutoSize = true;
			this->timeTaken->Font = (gcnew System::Drawing::Font(L"High Tower Text", 14, System::Drawing::FontStyle::Regular, System::Drawing::GraphicsUnit::Point, 
				static_cast<System::Byte>(0)));
			this->timeTaken->Location = System::Drawing::Point(875, 475);
			this->timeTaken->Name = L"timeTaken";
			this->timeTaken->Size = System::Drawing::Size(0, 22);
			this->timeTaken->TabIndex = 4;
			// 
			// feedback
			// 
			this->feedback->AutoSize = true;
			this->feedback->Font = (gcnew System::Drawing::Font(L"High Tower Text", 15, System::Drawing::FontStyle::Regular, System::Drawing::GraphicsUnit::Point, 
				static_cast<System::Byte>(0)));
			this->feedback->Location = System::Drawing::Point(838, 666);
			this->feedback->Name = L"feedback";
			this->feedback->Size = System::Drawing::Size(0, 23);
			this->feedback->TabIndex = 5;
			// 
			// parTime
			// 
			this->parTime->AutoSize = true;
			this->parTime->Font = (gcnew System::Drawing::Font(L"High Tower Text", 14, System::Drawing::FontStyle::Regular, System::Drawing::GraphicsUnit::Point, 
				static_cast<System::Byte>(0)));
			this->parTime->Location = System::Drawing::Point(876, 507);
			this->parTime->Name = L"parTime";
			this->parTime->Size = System::Drawing::Size(0, 22);
			this->parTime->TabIndex = 6;
			// 
			// Form1
			// 
			this->AutoScaleDimensions = System::Drawing::SizeF(7, 14);
			this->AutoScaleMode = System::Windows::Forms::AutoScaleMode::Font;
			this->BackColor = System::Drawing::Color::Black;
			this->ClientSize = System::Drawing::Size(1024, 768);
			this->Controls->Add(this->parTime);
			this->Controls->Add(this->feedback);
			this->Controls->Add(this->timeTaken);
			this->Controls->Add(this->currentLevelName);
			this->Controls->Add(this->controls);
			this->Controls->Add(this->controlTitleLabel);
			this->Controls->Add(this->titleLabel);
			this->Controls->Add(this->gamePanel);
			this->DoubleBuffered = true;
			this->Font = (gcnew System::Drawing::Font(L"High Tower Text", 8.25F, System::Drawing::FontStyle::Regular, System::Drawing::GraphicsUnit::Point, 
				static_cast<System::Byte>(0)));
			this->ForeColor = System::Drawing::Color::White;
			this->FormBorderStyle = System::Windows::Forms::FormBorderStyle::None;
			this->Name = L"Form1";
			this->StartPosition = System::Windows::Forms::FormStartPosition::Manual;
			this->Text = L"Aiden";
			this->Load += gcnew System::EventHandler(this, &Form1::Form1_Load);
			this->KeyDown += gcnew System::Windows::Forms::KeyEventHandler(this, &Form1::Form1_KeyDown);
			this->KeyUp += gcnew System::Windows::Forms::KeyEventHandler(this, &Form1::Form1_KeyUp);
			this->ResumeLayout(false);
			this->PerformLayout();

		}
#pragma endregion

	private:
		Graphics^ panelGraphics;
		GameManager^ manager;

	public:
		Form1(void)
		{
			
			InitializeComponent();
		}

	private: System::Void Form1_Load(System::Object^  sender, System::EventArgs^  e) {
				 Height = WINDOW_HEIGHT;
				 Width = WINDOW_WIDTH;
				 gamePanel->Height = PANEL_HEIGHT;
				 gamePanel->Width = PANEL_WIDTH;
				 panelGraphics = gamePanel->CreateGraphics();

				 //Put the labels into an array
				 array<Label^>^ labels = gcnew array<Label^>(NUMBER_OF_LABELS);
				 labels[ELabelArrayLocations::Feedback] = feedback;
				 labels[ELabelArrayLocations::CurrentTime] = timeTaken;
				 labels[ELabelArrayLocations::LevelName] = currentLevelName;
				 labels[ELabelArrayLocations::ParTime] = parTime;
				 array<String^>^ levelNames = gcnew array<String^>(NUMBER_OF_LEVELS);
				 levelNames[0] = "Escape";
				 manager = gcnew GameManager(panelGraphics, ClientSize, levelNames, labels);
				 gameTimer->Enabled = true;
			 }

	private: System::Void Form1_KeyDown(System::Object^  sender, System::Windows::Forms::KeyEventArgs^  e) {
				 manager->OnKeyDown(e->KeyCode);
			 }

	private: System::Void gameTimer_Tick(System::Object^  sender, System::EventArgs^  e) {
				 manager->OnTick();
			 }

	private: System::Void Form1_KeyUp(System::Object^  sender, System::Windows::Forms::KeyEventArgs^  e) {
				 manager->OnKeyUp(e->KeyCode);
			 }

	private: System::Void gamePanel_MouseClick(System::Object^  sender, System::Windows::Forms::MouseEventArgs^  e) {
				  manager->OnMouseClick(e);
			 }
};
}

