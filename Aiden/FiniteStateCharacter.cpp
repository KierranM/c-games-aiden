#include "stdafx.h"
#include "FiniteStateCharacter.h"

FiniteStateCharacter::FiniteStateCharacter() :Character(){
	//Empty default constructor
}//End constructor no params

FiniteStateCharacter::FiniteStateCharacter(
		array<String^, TWO>^ filenames, 
		array<int,TWO>^ frameNumbers, 
		array<Size>^ frameSize,
		Point startLocation,
		int startAction,
		int startXDeadZone,
		int startYDeadZone,
		Graphics^ startCanvas,
		Random^ startRandom,
		Rectangle startBounds,
		TileMap^ startMap,
		ObjectMap^ startObjects,
		bool startCanBePossessed,
		int startWanderingTime,
		int startWanderingTicks,
		int startWorkingTime,
		int startWorkingTicks
	)
		:Character(filenames,frameNumbers,frameSize,startLocation,startAction,FSM_DIRECTIONS, startXDeadZone,startYDeadZone,startCanvas, startRandom,startBounds,startMap,startObjects)
{
	//Set up the addition fields using the given values
	canBePossessed = startCanBePossessed;
	wanderingTime = startWanderingTime;
	wanderingTicks = startWanderingTicks;
	workingTime = startWorkingTime;
	workingTicks = startWanderingTicks;

	//Set up the addition fields using the default values
	currentDirection = eDirection::Left;
	wasClicked = false;
	isPossessed = false;
	interactionTicks = 0;
	deathAdjustment = false;
}//End constructor 16 params

void FiniteStateCharacter::UpdateState(AidenCharacter^ player, BitArray^ keys)
{
	//Empty default UpdateState
}//End UpdateState(player, keys)

void FiniteStateCharacter::PerformActions(AidenCharacter^ player, BitArray^ keys)
{
	//Empty default PerformActions
}//End PerformActions(player,keys)

void FiniteStateCharacter::MoveToTopOfTile()
{
	//Moves the FiniteStateCharacter to the top of the tile below them

	//Get the maps tilesize
	int tileSize = map->GetTileSize();
	
	//Get the height of the current frame
	int frameHeight = GetFrameSize().Height;
	//Calculate the yIndex of the tile
	int tileY = (((location.Y + frameHeight) + velocity.Y)/tileSize);
	//Calculate the pixel location at the top of that tile
	int tileYTop = tileY * tileSize;
	//Set the y values of the location and hitbox
	hitbox.Y = (tileYTop - TWO) - hitbox.Height;
	location.Y = (tileYTop - TWO) - (hitbox.Height - yDeadzone);
}//End MoveToTopOfTile()

bool FiniteStateCharacter::StandingOnAir()
{
	//Checks if the FiniteStateCharacter is standing on a non-solid tile
	bool onAir = true;

	//Calculate the indices of the tiles at the left and right sides of the bottom of the sprite
	int tileSize = map->GetTileSize();
	int leftTileX = location.X/tileSize;
	int tileY = ((location.Y + GetHitBox().Height) + velocity.Y)/tileSize;
	int rightTileX = (location.X + GetHitBox().Width)/tileSize;

	//check if the tiles or objects in these locations are solid
	if ((map->GetIsSolid(leftTileX, tileY) || map->GetIsSolid(rightTileX, tileY)) || (objects->GetIsSolid(leftTileX, tileY) || objects->GetIsSolid(rightTileX, tileY)))
	{
		//A tile was solid
		onAir = false;
	}

	return onAir;
}//End StandingOnAir()

bool FiniteStateCharacter::PossessedNPCInLOS(AidenCharacter^ player, int viewDistance, bool requireEyeLevel){
	//Checks if a possessed NPC is within range of the current NPC
	bool inLOS = true;

	//Check if the player is not possessing anyone
	if (player->GetPossessing() == nullptr)
	{
		inLOS = false;
	}
	else //Someone is being possessed
	{
		//Get the possessed NPC
		Character^ possessed = player->GetPossessing();

		//Check if the possessed NPC is is at all on the eye level of the sprite
		int myEye = hitbox.Y + EYE_LEVEL;
		Rectangle otherGuysHB = possessed->GetHitBox();

		if (requireEyeLevel && (otherGuysHB.Y > myEye || otherGuysHB.Bottom < myEye))
		{
			inLOS = false;
		}
		else //The Possessed NPC within the eye level of the current NPC
		{
			//Calculate the indices for the tile map
			int tileSize = map->GetTileSize();
			int tileY = hitbox.Y/tileSize;
			int tileX = hitbox.X/tileSize;
			bool wall = false;
			bool player = false;
			int count = 0;
			
			//Loop until either a solid wall or player is found and the count has not exceeded the view distance
			while (wall == false && player == false && count < viewDistance)
			{
				//Calculate the point to check
				int xVal = hitbox.X;
				if (currentDirection == eDirection::Left)
				{
					xVal -= count;
				}
				else{
					xVal += count;
				}
				Point p = Point(xVal, hitbox.Y);

				//Recalculate tileX
				tileX = xVal/tileSize;
				
				//Check if the tile at the current point is solid
				if (map->GetIsSolid(tileX,tileY) || objects->GetIsSolid(tileX,tileY))
				{
					wall = true;
				}

				//Check if the point is contained within the hitbox of the possessed NPC
				if (otherGuysHB.Contains(p))
				{
					player = true;
				}

				//Increase count
				count++;
			}//End while

			//No player was found
			if (!player)
			{
				inLOS = false;
			}//End no player if
		}//End possessed NPC in eyelevel else
	}//End there is a possessed NPC if

	return inLOS;
}//End PossessedNPCInLOS(player)

Distance FiniteStateCharacter::DistanceFrom(Character^ character)
{
	//Calculates the distance and direction of the current NPC to the given Character

	//Get the two characters center points
	Point ogCenter = character->GetCenter();
	Point myCenter = GetCenter();

	//Calculate the hypotinuse between the current NPC's center and the given characters
	double a = ogCenter.X - myCenter.X;
	double b = ogCenter.Y - myCenter.Y;
	double c = Math::Sqrt(Math::Pow(a,2) + Math::Pow(b,2));

	//Create the object to hold the information
	Distance d = Distance();
	d.distance = Math::Abs(c);

	//Get the horizontal direction to the given character
	if (a < 0)
	{
		d.direction = eDirection::Left;
	}
	else//The given character is to the right
	{
		d.direction = eDirection::Right;
	}
	return d;
}//End DistanceFrom(character)

void FiniteStateCharacter::Draw(Point viewportWorldLocation)
{
	//Get the tile at the center of the sprite
	Point center = GetCenter();
	int centerTileX = center.X/map->GetTileSize();
	int centerTileY = center.Y/map->GetTileSize();
	Point characterViewportLocation = CalculateViewPortLocation(viewportWorldLocation);

	//Draws the sprite at the given location
	canvas->DrawImage(spriteSheets[currentAction, currentDirection]->GetFrame(), characterViewportLocation.X, characterViewportLocation.Y);

	//Display the sprites hitbox when in debug mode
	#ifdef _DEBUG
		canvas->DrawRectangle(Pens::Black, characterViewportLocation.X + xDeadzone, characterViewportLocation.Y + yDeadzone, hitbox.Width, hitbox.Height);
		int hitX = hitbox.X - viewportWorldLocation.X;
		int hitY = hitbox.Y - viewportWorldLocation.Y;
		canvas->DrawRectangle(Pens::Blue, hitX, hitY,hitbox.Width, hitbox.Height);
	#endif
}//End Draw(viewportWorldLocation)

void FiniteStateCharacter::Move()
{
	//Moves the NPC using the current direction

	//Create a temporary Rectangle
	Rectangle onMove = hitbox;

	//Modify the X value by velocity.X depending on the direction
	if (currentDirection == eDirection::Left)
	{
		onMove.X -= velocity.X;
	}
	else
	{
		onMove.X += velocity.X;
	}

	//Increase the Y value by the y velocity
	onMove.Y += velocity.Y;
	
	//Is the horizontal tile solid
	if (!CheckTileX(onMove))
	{
		//Is it off the left or right edge
		if (!CheckHitEdgeX(onMove))
		{
			//It is a valid move so do the horizontal move
			hitbox.X = onMove.X;
			if (currentDirection == eDirection::Left)
			{
				location.X -= velocity.X;
			}
			else
			{
				location.X += velocity.X;
			}//End move Right if
		}//End outofbounds if
	}//End check tile X
	else
	{
		//The NPC has hit a solid tile so turn around
		currentDirection = (eDirection)((currentDirection+1)%TWO);
	}

	//Is the vertical tile solid
	if(!CheckTileY(onMove))
	{
		//Is it off the top or bottom edge
		if (!CheckHitEdgeY(onMove))
		{
			//The vertical move is valid so do it
			hitbox.Y = onMove.Y;
			location.Y += velocity.Y;
		}//End outofbounds if
	}//End check tile Y if
}//End Move()

bool FiniteStateCharacter::Contains(Point p)
{
	//Checks if the characters hitbox contains the given point
	bool contains = false;
	if(hitbox.Contains(p))
	{
		contains = true;
	}

	return contains;
}//End Contains(p)

void FiniteStateCharacter::PossessedActions(BitArray^ keys)
{
	//Does the movements for a possessed NPC, based off the keys that are currently down
	
	//Set a bool to indicate if any of the movement keys are pressed
	bool moveKey = false;

	//Is the A key down
	if (keys->Get((int)Keys::A))
	{
		//Move left
		currentDirection = eDirection::Left;
		moveKey = true;
	}
	
	//Is the D key down
	if (keys->Get((int)Keys::D))
	{
		//Move right
		currentDirection = eDirection::Right;
		moveKey = true;
	}

	//Check if the player is interacting with the environment
	if (keys->Get((int)Keys::E) && interactionTicks > INTERACTION_TIME)
	{
		//Get the height and width of the current frame
		int frameWidth = GetFrameSize().Width;
		int frameHeight = GetFrameSize().Height;

		//Create an array to hold the points along the side
		array<Point>^ side = gcnew array<Point>(frameHeight);

		//Get the value of the left side
		int x = location.X;

		//If the NPC is heading right then add the width to move X to be the value of the right side
		if (currentDirection == eDirection::Right)
		{
			x = location.X + frameWidth;
		}

		//Get all the points along its side
		for (int i = 0; i < frameHeight; i++)
		{
			int y = location.Y + i;
			side[i] = Point(x,y);
		}

		//Create a pointer to keep track of the last triggered tile
		AnimatedTile^ lastTile;

		//Get the size of the tiles
		int tileSize = objects->GetTileSize();

		//Loop through points
		for (int i = 0; i < side->Length; i++)
		{
			int tileX = side[i].X/tileSize;
			int tileY = side[i].Y/tileSize;
			//Get the tile
			AnimatedTile^ tile = objects->GetTile(tileX, tileY);
			
			//Only continue if the current tile is not the last tile that was triggered
			if (tile != lastTile)
			{
				//Trigger the tile
				tile->Trigger();

				//Remember the tile that was triggered
				lastTile = tile;
			}
		}

		//Update all the tiles
		objects->UpdateAll();

		//Reset the number of interaction ticks
		interactionTicks = 0;
	}//End of interaction if

	//Was a movement key down
	if (moveKey)
	{
		Move();
	}//End move if
}//End PossessedActions(keys)

void FiniteStateCharacter::WanderPatrol()
{
	//Make the NPC wander/patrol the area
	wanderingTicks++;
	Move();
}//End WanderPatrol

void FiniteStateCharacter::WorkPaused()
{
	//Make the NPC stand still to be Working/Paused
	workingTicks++;
}//End WorkPaused()

void FiniteStateCharacter::Fall(BitArray^ keys)
{
	//Make the character fall

	//Add gravity to the current velocity
	velocity.Y += GRAVITY;
	location.Y += velocity.Y;
	hitbox.Y += velocity.Y;
	Rectangle temp = hitbox;
	
	//When possessed the player can move side to side
	if(isPossessed)
	{
		//Is the A key pressed
		if (keys->Get((int)Keys::A))
		{
			//Move left
			currentDirection = eDirection::Left;
			temp.X -= velocity.X;
		}
		else// A is not pressed
		{
			//Is the D key pressed
			if (keys->Get((int)Keys::D))
			{
				//move right
				currentDirection = eDirection::Right;
				temp.X += velocity.X;
			}//End D key if
		}//end not a else
	}//End is possessed if

	//Is the horizontal tile not solid
	if (!CheckTileX(temp))
	{
		//Is the horizontal move not out of bounds
		if(!CheckHitEdgeX(temp))
		{
			//The move was valid
			location.X = temp.X - xDeadzone;
			hitbox = temp;
		}//End outofbounds if
	}//End solid tile if

	//If the NPC has hit a solid tile vertically or hit the edge
	if(CheckTileY(temp) || CheckHitEdgeY(temp))
	{
		//Stop the vertical movement
		velocity.Y = 0;
	}

	//Is the NPC no longer standing on air
	if(!StandingOnAir())
	{
		landed = true;
		MoveToTopOfTile();
		velocity.Y = 0;
	}//End not standingonair if
}//End Fall(keys)

void FiniteStateCharacter::OnDeath()
{
	//Updates the hitbox and moves the NPC down to the nearest solid tile
	//Rotate the hitbox
	Size s = GetFrameSize();
	int newX = location.X + xDeadzone;
	int newY = location.Y + yDeadzone;
	int newWidth = s.Width - (xDeadzone*TWO);
	int newHeight = s.Height - (yDeadzone*TWO);

	hitbox = Rectangle(newX,newY,newWidth, newHeight);

	//Get the bottom left of the hitbox
	Point p = Point(hitbox.X, hitbox.Bottom);

	//Get the size of the tiles in the map
	int tileSize = map->GetTileSize();
	int tileX;
	int tileY;
	//Loop until the tile at p is solid
	do
	{
		//Calculate the indices of the tile in the map
		tileX = p.X/tileSize;
		tileY = p.Y/tileSize;

		//Increase the y location
		p.Y++;
	} while (!map->GetIsSolid(tileX,tileY) && !objects->GetIsSolid(tileX, tileY));

	//Assign the Y value to the hitbox
	location.Y = p.Y - hitbox.Height;
	//Move the NPC to the top of the tile
	MoveToTopOfTile();
	//Set a bool so this only runs once
	deathAdjustment = true;
}//End OnDeath()