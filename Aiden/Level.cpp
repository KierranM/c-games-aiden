#include "stdafx.h"
#include "Level.h"


Level::Level(
	String^ startFolderPath,
	Graphics^ startCanvas, 
	Random^ startRand
)
{
	//Sets up the new Level using the given parameters and the default values

	//Store the given values
	canvas = startCanvas;
	rand = startRand;
	folderPath = startFolderPath;

	//Create the list of NPC's
	levelNPCS = gcnew FSMCharacterList();
	

	//Load up the level.dat file
	try
	{
		//Open the stream to the level.dat file that contains the information about the level
		StreamReader^ reader = gcnew StreamReader(folderPath + "/level.dat");
		//The level name is the first line of the file
		levelName = reader->ReadLine();

		//The par time is the second line
		parTime = Convert::ToInt32(reader->ReadLine());

		//The number of tile rows in the level is the third line
		int rows = Convert::ToInt32(reader->ReadLine());

		//The number of the tile columns in the level is the fourth line
		int columns = Convert::ToInt32(reader->ReadLine());

		//The size of the tiles is the fifth line
		int tileSize = Convert::ToInt32(reader->ReadLine());

		//Create the world rectangle based off the number of columns and rows, and the tile size
		int worldWidth = columns*tileSize;
		int worldHeight = rows*tileSize;
		world = Rectangle(0,0,worldWidth,worldHeight);

		//The name of the background image for this level is the sixth
		background = gcnew Bitmap(folderPath + reader->ReadLine());

		//The start location of the player is the seventh line
		String^ line = reader->ReadLine();
		array<String^>^ parts = line->Split(',');
		int pStartX = Convert::ToInt32(parts[0]);
		int pStartY = Convert::ToInt32(parts[1]);
		playerStart = Point(pStartX, pStartY);

		//The number of tiles in the map is the eighth line
		int numTileTypes = Convert::ToInt32(reader->ReadLine());

		//The next n number of lines contain the information for the different tiles. n being the value above
		TileList^ list = gcnew TileList(numTileTypes);

		//loop for the number of tile types
		for (int i = 0; i < numTileTypes; i++)
		{
			//Read a line describing a tile from the file
			String^ line = reader->ReadLine();

			//Split the line into its comma delimited parts
			array<String^>^ parts = line->Split(',');

			//use the parts to set the values
			String^ tileImagePath =folderPath + parts[TILE_IMAGE_STRING];
			bool isTileLethal = Convert::ToBoolean(parts[TILE_IS_LETHAL]);
			bool isTileSolid = Convert::ToBoolean(parts[TILE_IS_SOLID]);
			bool isAidenBarrier = Convert::ToBoolean(parts[TILE_IS_AIDEN_BARRIER]);
			bool tileTransparent = Convert::ToBoolean(parts[TILE_TRANSPARENT]);

			//Create the tile
			Tile^ t = gcnew Tile(tileImagePath, isTileLethal, isTileSolid, isAidenBarrier);

			//If the tile should be transparent then make it so
			if (tileTransparent)
			{
				t->SetTileTransparency(Color::White);
			}

			//Add the tile to the list
			list->SetTile(t,i);
		}//End of for
		
		//Create the tilemap
		map = gcnew TileMap(rows, columns, tileSize, list, canvas);

		//Load the tilemap from the map.csv file
		map->LoadFromFile(folderPath + "/map.csv");
		//Done loading the tilemap

		//Load the objects map
		objectMap = gcnew ObjectMap(rows, columns, tileSize);
		objectMap->LoadFromFile(folderPath);
		objectMap->LoadTriggersFromFile(folderPath);

		//Create the list for projectiles
		bulletList = gcnew ProjectileList(map,objectMap,"bullet.png", canvas,world);

		//Now until the end of the file are the NPC's
		while (!reader->EndOfStream)
		{
			//Read a line describing an NPC
			String^ line = reader->ReadLine();

			//Split the line into its comma delimited parts
			array<String^>^ parts = line->Split(',');

			//Get the information from the parts
			NPCTypes::eCharacterTypes npcType = (NPCTypes::eCharacterTypes)Convert::ToInt32(parts[0]);
			int xLoc =  Convert::ToInt32(parts[X_LOCATION]);
			int yLoc =  Convert::ToInt32(parts[Y_LOCATION]);

			//Add an NPC to the list depending on its NPC type
			switch (npcType)
			{
			case NPCTypes::eCharacterTypes::Scientist:
				SpawnScientist(Point(xLoc,yLoc));
				break;
			case NPCTypes::eCharacterTypes::Maintenance:
				SpawnMechanic(Point(xLoc,yLoc));
				break;
			case NPCTypes::eCharacterTypes::Guard:
				SpawnGuard(Point(xLoc,yLoc));
				break;
			case NPCTypes::eCharacterTypes::Special_Guard:
				SpawnSpecialGuard(Point(xLoc,yLoc));
				break;
			default:
				break;
			}//End switch statement
		}//End for loop
		//Close the file stream
		reader->Close();
	}//End try statement
	catch (Exception^ e)
	{
		//An error occured while loading the level
		MessageBox::Show("There was an error loading the level\n" + e->Message);
	}//end catch
}//End constructor 3 params

void Level::OnClick(Point p)
{
	//Used for debugging map triggers
	objectMap->TriggerTileAtPoint(p);
	objectMap->UpdateAll();
}//End OnClick(p)

void Level::SpawnScientist(Point startLocation)
{
	//Spawns a scientist by setting up the correct filenames and values, and adding it to the NPC list
	array<String^,TWO>^ scientistFilenames = gcnew array<String^,TWO>(CIVILIAN_STATES, FSM_DIRECTIONS);
	
	//Wandering
	scientistFilenames[CivilianStates::ECharacterState::Wandering, eDirection::Left] = "./Characters/Scientist/move-left.png";
	scientistFilenames[CivilianStates::ECharacterState::Wandering, eDirection::Right] = "./Characters/Scientist/move-right.png";

	//Working
	scientistFilenames[CivilianStates::ECharacterState::Working, eDirection::Left] = "./Characters/Scientist/work.png";
	scientistFilenames[CivilianStates::ECharacterState::Working, eDirection::Right] = "./Characters/Scientist/work.png";

	//Fleeing
	scientistFilenames[CivilianStates::ECharacterState::Fleeing, eDirection::Left] = "./Characters/Scientist/move-left.png";
	scientistFilenames[CivilianStates::ECharacterState::Fleeing, eDirection::Right] = "./Characters/Scientist/move-right.png";

	//Possessed
	scientistFilenames[CivilianStates::ECharacterState::Possessed, eDirection::Left] = "./Characters/Scientist/possessed-left.png";
	scientistFilenames[CivilianStates::ECharacterState::Possessed, eDirection::Right] = "./Characters/Scientist/possessed-right.png";

	//Falling
	scientistFilenames[CivilianStates::ECharacterState::Falling, eDirection::Left] = "./Characters/Scientist/fall-left.png";
	scientistFilenames[CivilianStates::ECharacterState::Falling, eDirection::Right] = "./Characters/Scientist/fall-right.png";

	//Dead
	scientistFilenames[CivilianStates::ECharacterState::Dead, eDirection::Left] = "./Characters/Scientist/dead-left.png";
	scientistFilenames[CivilianStates::ECharacterState::Dead, eDirection::Right] = "./Characters/Scientist/dead-right.png";

	//Set up the number of frames and the size of those frames
	array<int,TWO>^ scientistFrameNumbers = gcnew array<int,TWO>(CIVILIAN_STATES,FSM_DIRECTIONS);
	array<Size>^ scientistFrameSize = gcnew array<Size>(CIVILIAN_STATES);
	
	scientistFrameNumbers[CivilianStates::ECharacterState::Wandering, eDirection::Left] = SCIENTIST_FRAMES;
	scientistFrameNumbers[CivilianStates::ECharacterState::Wandering, eDirection::Right] = SCIENTIST_FRAMES;
	scientistFrameSize[CivilianStates::ECharacterState::Wandering] = Size(SCIENTIST_WIDTH, SCIENTIST_HEIGHT);

	scientistFrameNumbers[CivilianStates::ECharacterState::Working, eDirection::Left] = SCIENTIST_FRAMES;
	scientistFrameNumbers[CivilianStates::ECharacterState::Working, eDirection::Right] = SCIENTIST_FRAMES;
	scientistFrameSize[CivilianStates::ECharacterState::Working] = Size(SCIENTIST_WIDTH, SCIENTIST_HEIGHT);

	scientistFrameNumbers[CivilianStates::ECharacterState::Fleeing, eDirection::Left] = SCIENTIST_FRAMES;
	scientistFrameNumbers[CivilianStates::ECharacterState::Fleeing, eDirection::Right] = SCIENTIST_FRAMES;
	scientistFrameSize[CivilianStates::ECharacterState::Fleeing] = Size(SCIENTIST_WIDTH, SCIENTIST_HEIGHT);

	scientistFrameNumbers[CivilianStates::ECharacterState::Possessed, eDirection::Left] = SCIENTIST_FRAMES;
	scientistFrameNumbers[CivilianStates::ECharacterState::Possessed, eDirection::Right] = SCIENTIST_FRAMES;
	scientistFrameSize[CivilianStates::ECharacterState::Possessed] = Size(SCIENTIST_WIDTH, SCIENTIST_HEIGHT);

	scientistFrameNumbers[CivilianStates::ECharacterState::Falling, eDirection::Left] = SCIENTIST_FRAMES;
	scientistFrameNumbers[CivilianStates::ECharacterState::Falling, eDirection::Right] = SCIENTIST_FRAMES;
	scientistFrameSize[CivilianStates::ECharacterState::Falling] = Size(SCIENTIST_WIDTH, SCIENTIST_HEIGHT);

	scientistFrameNumbers[CivilianStates::ECharacterState::Dead, eDirection::Left] = SCIENTIST_FRAMES;
	scientistFrameNumbers[CivilianStates::ECharacterState::Dead, eDirection::Right] = SCIENTIST_FRAMES;
	scientistFrameSize[CivilianStates::ECharacterState::Dead] = Size(SCIENTIST_HEIGHT, SCIENTIST_WIDTH);

	//Add the scientist to the list
	levelNPCS->Add(gcnew Civilian(scientistFilenames,scientistFrameNumbers,scientistFrameSize,startLocation,CivilianStates::ECharacterState::Wandering,SCIENTIST_DEADZONE,SCIENTIST_DEADZONE,canvas,rand,world,map,objectMap,SCIENTIST_WANDERING_TICKS,0,SCIENTIST_WORKING_TICKS,0));
}//End SpawnScientist(startLocation)

void Level::SpawnMechanic(Point startLocation)
{
	//Spawns a scientist by setting up the correct filenames and values, and adding it to the NPC list

	//Create the array of filenames
	array<String^,TWO>^ mechanicFilenames = gcnew array<String^,TWO>(CIVILIAN_STATES, FSM_DIRECTIONS);
	
	//Wandering
	mechanicFilenames[CivilianStates::ECharacterState::Wandering, eDirection::Left] = "./Characters/Maintenance Worker/move-left.png";
	mechanicFilenames[CivilianStates::ECharacterState::Wandering, eDirection::Right] = "./Characters/Maintenance Worker/move-right.png";

	//Working
	mechanicFilenames[CivilianStates::ECharacterState::Working, eDirection::Left] = "./Characters/Maintenance Worker/work-left.png";
	mechanicFilenames[CivilianStates::ECharacterState::Working, eDirection::Right] = "./Characters/Maintenance Worker/work-right.png";

	//Fleeing
	mechanicFilenames[CivilianStates::ECharacterState::Fleeing, eDirection::Left] = "./Characters/Maintenance Worker/move-left.png";
	mechanicFilenames[CivilianStates::ECharacterState::Fleeing, eDirection::Right] = "./Characters/Maintenance Worker/move-right.png";

	//Possessed
	mechanicFilenames[CivilianStates::ECharacterState::Possessed, eDirection::Left] = "./Characters/Maintenance Worker/possessed-left.png";
	mechanicFilenames[CivilianStates::ECharacterState::Possessed, eDirection::Right] = "./Characters/Maintenance Worker/possessed-right.png";

	//Falling
	mechanicFilenames[CivilianStates::ECharacterState::Falling, eDirection::Left] = "./Characters/Maintenance Worker/fall-left.png";
	mechanicFilenames[CivilianStates::ECharacterState::Falling, eDirection::Right] = "./Characters/Maintenance Worker/fall-right.png";

	//Dead
	mechanicFilenames[CivilianStates::ECharacterState::Dead, eDirection::Left] = "./Characters/Maintenance Worker/dead-left.png";
	mechanicFilenames[CivilianStates::ECharacterState::Dead, eDirection::Right] = "./Characters/Maintenance Worker/dead-right.png";

	//Set up the number of frames and the size of those frames
	array<int,TWO>^ mechanicFrameNumbers = gcnew array<int,TWO>(CIVILIAN_STATES,FSM_DIRECTIONS);
	array<Size>^ mechanicFrameSize = gcnew array<Size>(CIVILIAN_STATES);
	
	mechanicFrameNumbers[CivilianStates::ECharacterState::Wandering, eDirection::Left] = MECHANIC_FRAMES;
	mechanicFrameNumbers[CivilianStates::ECharacterState::Wandering, eDirection::Right] = MECHANIC_FRAMES;
	mechanicFrameSize[CivilianStates::ECharacterState::Wandering] = Size(MECHANIC_WIDTH, MECHANIC_HEIGHT);

	mechanicFrameNumbers[CivilianStates::ECharacterState::Working, eDirection::Left] = MECHANIC_FRAMES;
	mechanicFrameNumbers[CivilianStates::ECharacterState::Working, eDirection::Right] = MECHANIC_FRAMES;
	mechanicFrameSize[CivilianStates::ECharacterState::Working] = Size(MECHANIC_WIDTH, MECHANIC_HEIGHT);

	mechanicFrameNumbers[CivilianStates::ECharacterState::Fleeing, eDirection::Left] = MECHANIC_FRAMES;
	mechanicFrameNumbers[CivilianStates::ECharacterState::Fleeing, eDirection::Right] = MECHANIC_FRAMES;
	mechanicFrameSize[CivilianStates::ECharacterState::Fleeing] = Size(MECHANIC_WIDTH, MECHANIC_HEIGHT);

	mechanicFrameNumbers[CivilianStates::ECharacterState::Possessed, eDirection::Left] = MECHANIC_FRAMES;
	mechanicFrameNumbers[CivilianStates::ECharacterState::Possessed, eDirection::Right] = MECHANIC_FRAMES;
	mechanicFrameSize[CivilianStates::ECharacterState::Possessed] = Size(MECHANIC_WIDTH, MECHANIC_HEIGHT);

	mechanicFrameNumbers[CivilianStates::ECharacterState::Falling, eDirection::Left] = MECHANIC_FRAMES;
	mechanicFrameNumbers[CivilianStates::ECharacterState::Falling, eDirection::Right] = MECHANIC_FRAMES;
	mechanicFrameSize[CivilianStates::ECharacterState::Falling] = Size(MECHANIC_WIDTH, MECHANIC_HEIGHT);

	mechanicFrameNumbers[CivilianStates::ECharacterState::Dead, eDirection::Left] = MECHANIC_FRAMES;
	mechanicFrameNumbers[CivilianStates::ECharacterState::Dead, eDirection::Right] = MECHANIC_FRAMES;
	mechanicFrameSize[CivilianStates::ECharacterState::Dead] = Size(MECHANIC_HEIGHT, MECHANIC_WIDTH);

	//Add the mechanic to the list
	levelNPCS->Add(gcnew Civilian(mechanicFilenames,mechanicFrameNumbers,mechanicFrameSize,startLocation,CivilianStates::ECharacterState::Wandering,MECHANIC_DEADZONE,MECHANIC_DEADZONE,canvas,rand,world,map,objectMap,SCIENTIST_WANDERING_TICKS,0,SCIENTIST_WORKING_TICKS,0));
}//End SpawnMechanic(startLocation)

void Level::SpawnGuard(Point startLocation)
{
	//Spawns a guard by setting up the correct filenames and values, and adding it to the NPC list

	//create the array of filenames
	array<String^,TWO>^ guardFilenames = gcnew array<String^,TWO>(GUARD_STATES, FSM_DIRECTIONS);
	
	//Patrolling
	guardFilenames[GuardStates::ECharacterState::Patrolling, eDirection::Left] = "./Characters/Guard/move-left.png";
	guardFilenames[GuardStates::ECharacterState::Patrolling, eDirection::Right] = "./Characters/Guard/move-right.png";

	//Paused
	guardFilenames[GuardStates::ECharacterState::Paused, eDirection::Left] = "./Characters/Guard/pause-left.png";
	guardFilenames[GuardStates::ECharacterState::Paused, eDirection::Right] = "./Characters/Guard/pause-right.png";

	//Pursuing
	guardFilenames[GuardStates::ECharacterState::Pursuing, eDirection::Left] = "./Characters/Guard/move-left.png";
	guardFilenames[GuardStates::ECharacterState::Pursuing, eDirection::Right] = "./Characters/Guard/move-right.png";

	//Engaging
	guardFilenames[GuardStates::ECharacterState::Engaging, eDirection::Left] = "./Characters/Guard/shooting-left.png";
	guardFilenames[GuardStates::ECharacterState::Engaging, eDirection::Right] = "./Characters/Guard/shooting-right.png";

	//Possessed
	guardFilenames[GuardStates::ECharacterState::Possessed, eDirection::Left] = "./Characters/Guard/possessed-left.png";
	guardFilenames[GuardStates::ECharacterState::Possessed, eDirection::Right] = "./Characters/Guard/possessed-right.png";

	//Falling
	guardFilenames[GuardStates::ECharacterState::Falling, eDirection::Left] = "./Characters/Guard/fall-left.png";
	guardFilenames[GuardStates::ECharacterState::Falling, eDirection::Right] = "./Characters/Guard/fall-right.png";

	//Dead
	guardFilenames[GuardStates::ECharacterState::Dead, eDirection::Left] = "./Characters/Guard/dead-left.png";
	guardFilenames[GuardStates::ECharacterState::Dead, eDirection::Right] = "./Characters/Guard/dead-right.png";

	//Set up the number of frames and the size of those frames
	array<int,TWO>^ guardFrameNumbers = gcnew array<int,TWO>(GUARD_STATES,FSM_DIRECTIONS);
	array<Size>^ guardFrameSize = gcnew array<Size>(GUARD_STATES);
	
	guardFrameNumbers[GuardStates::ECharacterState::Patrolling, eDirection::Left] = GUARD_FRAMES;
	guardFrameNumbers[GuardStates::ECharacterState::Patrolling, eDirection::Right] = GUARD_FRAMES;
	guardFrameSize[GuardStates::ECharacterState::Patrolling] = Size(GUARD_WIDTH, GUARD_HEIGHT);

	guardFrameNumbers[GuardStates::ECharacterState::Paused, eDirection::Left] = GUARD_FRAMES;
	guardFrameNumbers[GuardStates::ECharacterState::Paused, eDirection::Right] = GUARD_FRAMES;
	guardFrameSize[GuardStates::ECharacterState::Paused] = Size(GUARD_WIDTH, GUARD_HEIGHT);

	guardFrameNumbers[GuardStates::ECharacterState::Pursuing, eDirection::Left] = GUARD_FRAMES;
	guardFrameNumbers[GuardStates::ECharacterState::Pursuing, eDirection::Right] = GUARD_FRAMES;
	guardFrameSize[GuardStates::ECharacterState::Pursuing] = Size(GUARD_WIDTH, GUARD_HEIGHT);

	guardFrameNumbers[GuardStates::ECharacterState::Engaging, eDirection::Left] = GUARD_FRAMES;
	guardFrameNumbers[GuardStates::ECharacterState::Engaging, eDirection::Right] = GUARD_FRAMES;
	guardFrameSize[GuardStates::ECharacterState::Engaging] = Size(GUARD_WIDTH, GUARD_HEIGHT);

	guardFrameNumbers[GuardStates::ECharacterState::Possessed, eDirection::Left] = GUARD_FRAMES;
	guardFrameNumbers[GuardStates::ECharacterState::Possessed, eDirection::Right] = GUARD_FRAMES;
	guardFrameSize[GuardStates::ECharacterState::Possessed] = Size(GUARD_WIDTH, GUARD_HEIGHT);

	guardFrameNumbers[GuardStates::ECharacterState::Falling, eDirection::Left] = GUARD_FRAMES;
	guardFrameNumbers[GuardStates::ECharacterState::Falling, eDirection::Right] = GUARD_FRAMES;
	guardFrameSize[GuardStates::ECharacterState::Falling] = Size(GUARD_WIDTH, GUARD_HEIGHT);

	guardFrameNumbers[GuardStates::ECharacterState::Dead, eDirection::Left] = GUARD_FRAMES;
	guardFrameNumbers[GuardStates::ECharacterState::Dead, eDirection::Right] = GUARD_FRAMES;
	guardFrameSize[GuardStates::ECharacterState::Dead] = Size(GUARD_HEIGHT, GUARD_WIDTH);

	//Add the Guard to the list
	levelNPCS->Add(gcnew Guard(guardFilenames,guardFrameNumbers, guardFrameSize, startLocation, GuardStates::ECharacterState::Patrolling,GUARD_DEADZONE_X, GUARD_DEADZONE_Y,canvas,rand,world,map,objectMap,GUARD_WANDERING_TICKS, 0, GUARD_PAUSED_TICKS,0,true,bulletList));
}//End SpawnGuard(startLocation)

void Level::SpawnSpecialGuard(Point startLocation)
{
	//Spawns a special Guard by setting up the correct filenames and values, and adding it to the NPC list

	//create the array of filenames
	array<String^,TWO>^ specialGuardFilenames = gcnew array<String^,TWO>(SPECIAL_GUARD_STATES, FSM_DIRECTIONS);
	
	//Patrolling
	specialGuardFilenames[SpecialGuardStates::ECharacterState::Patrolling, eDirection::Left] = "./Characters/Special Guard/move-left.png";
	specialGuardFilenames[SpecialGuardStates::ECharacterState::Patrolling, eDirection::Right] = "./Characters/Special Guard/move-right.png";

	//Paused
	specialGuardFilenames[SpecialGuardStates::ECharacterState::Paused, eDirection::Left] = "./Characters/Special Guard/pause-left.png";
	specialGuardFilenames[SpecialGuardStates::ECharacterState::Paused, eDirection::Right] = "./Characters/Special Guard/pause-right.png";

	//Pursuing
	specialGuardFilenames[SpecialGuardStates::ECharacterState::Pursuing, eDirection::Left] = "./Characters/Special Guard/move-left.png";
	specialGuardFilenames[SpecialGuardStates::ECharacterState::Pursuing, eDirection::Right] = "./Characters/Special Guard/move-right.png";

	//Engaging
	specialGuardFilenames[SpecialGuardStates::ECharacterState::Engaging, eDirection::Left] = "./Characters/Special Guard/shooting-left.png";
	specialGuardFilenames[SpecialGuardStates::ECharacterState::Engaging, eDirection::Right] = "./Characters/Special Guard/shooting-right.png";

	//Falling
	specialGuardFilenames[SpecialGuardStates::ECharacterState::Falling, eDirection::Left] = "./Characters/Special Guard/fall-left.png";
	specialGuardFilenames[SpecialGuardStates::ECharacterState::Falling, eDirection::Right] = "./Characters/Special Guard/fall-right.png";

	//Set up the number of frames and the size of those frames
	array<int,TWO>^ specialGuardFrameNumbers = gcnew array<int,TWO>(SPECIAL_GUARD_STATES,FSM_DIRECTIONS);
	array<Size>^ specialGuardFrameSize = gcnew array<Size>(SPECIAL_GUARD_STATES);
	
	specialGuardFrameNumbers[SpecialGuardStates::ECharacterState::Patrolling, eDirection::Left] = SPECIAL_GUARD_FRAMES;
	specialGuardFrameNumbers[SpecialGuardStates::ECharacterState::Patrolling, eDirection::Right] = SPECIAL_GUARD_FRAMES;
	specialGuardFrameSize[SpecialGuardStates::ECharacterState::Patrolling] = Size(SPECIAL_GUARD_WIDTH, SPECIAL_GUARD_HEIGHT);

	specialGuardFrameNumbers[SpecialGuardStates::ECharacterState::Paused, eDirection::Left] = SPECIAL_GUARD_FRAMES;
	specialGuardFrameNumbers[SpecialGuardStates::ECharacterState::Paused, eDirection::Right] = SPECIAL_GUARD_FRAMES;
	specialGuardFrameSize[SpecialGuardStates::ECharacterState::Paused] = Size(SPECIAL_GUARD_WIDTH, SPECIAL_GUARD_HEIGHT);

	specialGuardFrameNumbers[SpecialGuardStates::ECharacterState::Pursuing, eDirection::Left] = SPECIAL_GUARD_FRAMES;
	specialGuardFrameNumbers[SpecialGuardStates::ECharacterState::Pursuing, eDirection::Right] = SPECIAL_GUARD_FRAMES;
	specialGuardFrameSize[SpecialGuardStates::ECharacterState::Pursuing] = Size(SPECIAL_GUARD_WIDTH, SPECIAL_GUARD_HEIGHT);

	specialGuardFrameNumbers[SpecialGuardStates::ECharacterState::Engaging, eDirection::Left] = SPECIAL_GUARD_FRAMES;
	specialGuardFrameNumbers[SpecialGuardStates::ECharacterState::Engaging, eDirection::Right] = SPECIAL_GUARD_FRAMES;
	specialGuardFrameSize[SpecialGuardStates::ECharacterState::Engaging] = Size(SPECIAL_GUARD_WIDTH, SPECIAL_GUARD_HEIGHT);

	specialGuardFrameNumbers[SpecialGuardStates::ECharacterState::Falling, eDirection::Left] = SPECIAL_GUARD_FRAMES;
	specialGuardFrameNumbers[SpecialGuardStates::ECharacterState::Falling, eDirection::Right] = SPECIAL_GUARD_FRAMES;
	specialGuardFrameSize[SpecialGuardStates::ECharacterState::Falling] = Size(SPECIAL_GUARD_WIDTH, SPECIAL_GUARD_HEIGHT);


	//Add the SpecialGuard to the list
	levelNPCS->Add(gcnew SpecialGuard(specialGuardFilenames,specialGuardFrameNumbers, specialGuardFrameSize, startLocation, SpecialGuardStates::ECharacterState::Patrolling,SPECIAL_GUARD_DEADZONE_X, SPECIAL_GUARD_DEADZONE_Y,canvas,rand,world,map,objectMap,SPECIAL_GUARD_WANDERING_TICKS, 0, SPECIAL_GUARD_PAUSED_TICKS,0,bulletList));
}//End SpawnSpecialGuard(startLocation)