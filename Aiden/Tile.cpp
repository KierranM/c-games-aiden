#include "stdafx.h"
#include "Tile.h"

Tile::Tile()
{
	//Create a default tile
	tileImage = gcnew Bitmap(0,0);
	isLethal = false;
	isSolid = false;
	isAidenBarrier = false;
}//End constructor no params

Tile::Tile(String^ filename, bool startLethal, bool startIsSolid, bool startAidenBarrier)
{
	//Create a tile with the given parameters
	tileImage = gcnew Bitmap(filename);
	isLethal = startLethal;
	isSolid = startIsSolid;
	isAidenBarrier = startAidenBarrier;
}//End constructor 4 params
