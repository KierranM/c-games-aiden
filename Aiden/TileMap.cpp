#include "stdafx.h"
#include "TileMap.h"


TileMap::TileMap(int startRows, int startCols, int startTileSize, TileList^ startList, Graphics^ startCanvas)
{
	//Construct the tilemap
	rows = startRows;
	columns = startCols;
	tileSize = startTileSize;
	map = gcnew array<int, 2>(startRows, startCols);
	tiles = startList;
	canvas = startCanvas;
}//End constructor 5 params

Bitmap^ TileMap::GetMapCellBitmap(int c, int r)
{
	int tileType = map[r,c];
	Bitmap^ cell = tiles->GetTile(tileType);
	return cell;
}//End GetMapCellBitmap()

void TileMap::LoadFromFile(String^ filename)
{
	//Open the file for reading
	StreamReader^ file = gcnew StreamReader(filename);

	//Loads the tilemap from the given file
	for (int r = 0; r < rows; r++)
	{
		String^ line = file->ReadLine();
		array<String^>^ parts = line->Split(',');
		for (int c = 0; c < columns; c++)
		{
			map[r,c] = Convert::ToInt32(parts[c]);
		}//End column for
	}//End row for

	//Close the file
	file->Close();
}//End LoadFromFile()

void TileMap::Draw()
{
	//Draws the tilemap, used for testing tilemap
	for (int y = 0; y < rows; y++)
	{
		for (int x = 0; x < columns; x++)
		{
			int xPos = x * tileSize;
			int yPos = y * tileSize;
			int tileIndex = map[y,x];
			Bitmap^ t = tiles->GetTile(tileIndex);
			Rectangle destRect = Rectangle(xPos, yPos, tileSize, tileSize);
			canvas->DrawImage(t, destRect);
		}//End column for
	}//end row for
}//End Draw()
