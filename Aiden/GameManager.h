#pragma once
#include "Level.h"
#include "ViewPort.h"
#include "Character.h"
#include "AidenCharacter.h"
#include "FiniteStateCharacter.h"
#include "Civilian.h"
#include "FSMCharacterList.h"

#define SECONDS_IN_MINUTE 60
#define START_SCORE 100000
#define SECOND_OVER_SCORE_PENALTY 300
#define NPC_DEATH_SCORE_PENALTY 5000
#define SCORE_DECREMENT 100
#define SCORE_SUMMARY_POSITION_X 20
#define SCORE_SUMMARY_POSITION_Y 20


#define NUMBER_OF_KEYS 255
#define NUMBER_OF_LABELS 4

#define VIEWPORT_HEIGHT 12
#define VIEWPORT_WIDTH 13

#define AIDEN_ACTIONS 1
#define AIDEN_FRAMES 1
#define AIDEN_HEIGHT 128
#define AIDEN_WIDTH 64
#define AIDEN_SPEED 8
#define AIDEN_DEADZONE 10
#define POSSESS_DISTANCE 50

#define FONT_SIZE 20

#define PARALLAX_FACTOR 0.1
#define BACKGROUND_CORRECTION 7

using namespace System;
using namespace System::ComponentModel;
using namespace System::Collections;
using namespace System::Windows::Forms;
using namespace System::Data;
using namespace System::Drawing;


public enum ELabelArrayLocations
{
	Feedback,
	LevelName,
	CurrentTime,
	ParTime
};

/*
	Author: Kierran McPherson
	Date: 18/11/2013
	Purpose: This class defines the GameManager. It has ultimate control of the game and is responsible for loading levels,
			 performing the game cycle, and dealing with user input.
	Known Bugs:
*/
ref class GameManager
{
private:
	Graphics^ buffer;
	Graphics^ panelGraphics;
	Level^ currentLevel;
	ViewPort^ viewport;
	Bitmap^ offScreen;
	BitArray^ keys;
	Size clientSize;
	Random^ rand;
	Rectangle world;
	Rectangle bgOne;
	Rectangle bgTwo;
	array<Label^>^ labels;
	array<String^>^ levelNames;

	AidenCharacter^ aiden;
	FSMCharacterList^ npcs;
	ProjectileList^ projectiles;
	Font^ gameFont;
	DateTime started;

	int startScore;
	int endScore;
	int currentLevelNumber;

	bool gameover;
	bool hasWon;
public:
	GameManager(Graphics^ startFormGraphics, Size startClientSize, array<String^>^ startLevelNames, array<Label^>^ startLabels);

	void OnTick();
	
	void DoKeyEvents();
	void OnMouseClick(MouseEventArgs^ e);
	void DrawBackground();
	void CalculateScore();
	void StartLevel();

	//Input events
	void OnKeyDown(Keys e) {keys->Set((int)e,true);}
	void OnKeyUp(Keys e) {keys->Set((int)e,false);}

	//Gets and Sets
	void SetPanelGraphics(Graphics^ newPanelGraphics)	{panelGraphics = newPanelGraphics;}
};

