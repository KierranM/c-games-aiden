#pragma once
#include "Guard.h"

#define SPECIAL_GUARD_SPEED 12
#define SPECIAL_GUARD_VIEW_DISTANCE 800
#define SPECIAL_GUARD_ENGAGE_DISTANCE 500

//Inside namespace to prevent scope errors
namespace SpecialGuardStates{
	public enum ECharacterState
	{
		Patrolling,
		Paused,
		Pursuing,
		Engaging,
		Falling
	};
};
/*
	Author: Kierran McPherson
	Date: 21/11/2013
	Purpose: This class defines a SpecialGuard, which is a specialised version of the Guard that cannot die and is much more aggressive
	Known Bugs:
*/
ref class SpecialGuard :
public Guard
{
public:
	SpecialGuard(
		array<String^, TWO>^ filenames, 
		array<int,TWO>^ frameNumbers, 
		array<Size>^ frameSize,
		Point startLocation,
		int startAction,
		int startXDeadZone,
		int startYDeadZone,
		Graphics^ startCanvas,
		Random^ startRandom,
		Rectangle startBounds,
		TileMap^ startMap,
		ObjectMap^ startObjects,
		int startPatrollingTime,
		int startPatrollingTicks,
		int startPausedTime,
		int startPausedTicks,
		ProjectileList^ startBullets
	);

	virtual void UpdateState(AidenCharacter^ player, BitArray^ keys) override;
	virtual void PerformActions(AidenCharacter^ player, BitArray^ keys) override;
};

