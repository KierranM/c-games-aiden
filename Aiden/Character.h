#pragma once
#include "SpriteSheet.h"
#include "TileMap.h"
#include "ObjectMap.h"

#define TWO 2

using namespace System::Drawing;
using namespace System::Collections;
using namespace System::Data;
using namespace System;

public enum eDirection
{
	Left,
	Right
};

/*
	Author: Kierran McPherson
	Date: 18/11/2013
	Purpose: This defines a Character in the game. It can be drawn to the screen and
			 contains all the fields for a common Character
	Known Bugs:
			+ There is an issue with the Tile collision when jumping upwards into a solid tile
*/
ref class Character
{
protected:
	Graphics^ canvas;
	array<SpriteSheet^,TWO>^ spriteSheets;
	Random^ random;
	Point location;
	Point velocity;
	Rectangle boundary;
	Rectangle hitbox;
	TileMap^ map;
	ObjectMap^ objects;
	int xDeadzone;
	int yDeadzone;
	int currentAction;
	bool isAlive;

public:
	Character();
	Character(
		array<String^, TWO>^ filenames, 
		array<int,TWO>^ frameNumbers, 
		array<Size>^ frameSize,
		Point startLocation,
		int startAction,
		int numDirections,
		int startXDeadZone,
		int startYDeadZone,
		Graphics^ startCanvas,
		Random^ startRandom,
		Rectangle startBounds,
		TileMap^ startMap,
		ObjectMap^ startObjects
		);

	virtual void Draw();
	virtual void Draw(Point viewportWorldLocation);
	virtual void Update();
	virtual void Move();
	virtual bool CheckTileX(Rectangle toCheck);
	virtual bool CheckTileY(Rectangle toCheck);
	bool CheckHitEdgeX(Rectangle toCheck);
	bool CheckHitEdgeY(Rectangle toCheck);
	virtual Point GetCenter();
	Point CalculateViewPortLocation(Point viewportWorldPosition);

	//Sets and Gets
	Rectangle GetHitBox()	{return hitbox;}
	void SetHitBox(Rectangle hb)	{hitbox = hb;}

	Point GetLocation() {return location;}
	void SetLocation(Point newLocation)	{location = newLocation;}

	Point GetVelocity() {return velocity;}
	void SetVelocity(Point newVelocity)	{velocity = newVelocity;}

	bool GetIsAlive() {return isAlive;}
	virtual void SetIsAlive(bool alive)	{isAlive = alive;}
	
	void SetBoundary(Rectangle bounds)	{boundary = bounds;}

	virtual Size GetFrameSize()	{return spriteSheets[currentAction,0]->GetFrameSize();}
};

