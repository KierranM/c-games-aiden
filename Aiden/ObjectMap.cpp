#include "stdafx.h"
#include "ObjectMap.h"


ObjectMap::ObjectMap(int startRows, int startCols, int startTileSize)
{
	//Set up the ObjectMap using the given values
	rows = startRows;
	cols = startCols;
	tileSize = startTileSize;
	tiles = gcnew array<AnimatedTile^,2>(rows, cols);
}//End constructor 3 parameters

void ObjectMap::LoadFromFile(String^ filepath)
{
	//Attempts to load the object map from the objects.csv file for the level
	try
	{
		//Create the streamreader to the csv file
		StreamReader^ reader = gcnew StreamReader(filepath + "/objects.csv");

		//Create the filenames for the different tile images
		String^ air = filepath + "/objects/air.png";
		String^ swOff = filepath + "/objects/switch-off.png";
		String^ swOn = filepath + "/objects/switch-on.png";
		String^ doorTopClosed = filepath + "/objects/door-top-closed.png";
		String^ doorTopOpen = filepath + "/objects/door-top-open.png";
		String^ doorMidClosed = filepath + "/objects/door-mid-closed.png";
		String^ doorMidOpen = filepath + "/objects/door-mid-open.png";
		String^ doorBottomClosed = filepath + "/objects/door-bottom-closed.png";
		String^ doorBottomOpen = filepath + "/objects/door-bottom-open.png";
		String^ exitTopLeftClosed = filepath + "/objects/exit-top-left-closed.png";
		String^ exitTopLeftOpen = filepath + "/objects/exit-top-left-open.png";
		String^ exitTopRightClosed = filepath + "/objects/exit-top-right-closed.png";
		String^ exitTopRightOpen = filepath + "/objects/exit-top-right-open.png";
		String^ exitMidLeftClosed = filepath + "/objects/exit-mid-left-closed.png";
		String^ exitMidLeftOpen = filepath + "/objects/exit-mid-left-open.png";
		String^ exitMidRightClosed = filepath + "/objects/exit-mid-right-closed.png";
		String^ exitMidRightOpen = filepath + "/objects/exit-mid-right-open.png";
		String^ exitBottomLeftClosed = filepath + "/objects/exit-bottom-left-closed.png";
		String^ exitBottomLeftOpen = filepath + "/objects/exit-bottom-left-open.png";
		String^ exitBottomRightClosed = filepath + "/objects/exit-bottom-right-closed.png";
		String^ exitBottomRightOpen = filepath + "/objects/exit-bottom-right-open.png";

		//Loop for the number of rows in the map
		for (int row = 0; row < rows; row++)
		{
			//Read a line from the file
			String^ line = reader->ReadLine();

			//Split the line on a colon to get the column values
			array<String^>^ lineValues = line->Split(',');

			//Loop for the number of columns in the map
			for (int col = 0; col < cols; col++)
			{
				//Get the current columns value
				int value = Convert::ToInt32(lineValues[col]);

				AnimatedTile^ at;
				//Switch on the value cast to an EOjectType enun value
				switch ((EObjectType)value)
				{
				case EObjectType::Air:
					//Set 'at' as an air tile
					at = gcnew AnimatedTile(air, air, false, false, false, false, false,false,false);
					break;
				case EObjectType::Switch:
					//Set 'at' as an switch tile
					at = gcnew AnimatedTile(swOff,swOn, false, false, false, false, false,true,false);
					break;
				case EObjectType::DoorTop:
					//Set 'at' as an door top tile
					at = gcnew AnimatedTile(doorTopClosed, doorTopOpen, false, true, false, true, true,false,false);
					break;
				case EObjectType::DoorMid:
					//Set 'at' as an door middle tile
					at = gcnew AnimatedTile(doorMidClosed,doorMidOpen, false, false, false, true, true,false,false);
					break;
				case EObjectType::DoorBottom:
					//Set 'at' as an door bottom tile
					at = gcnew AnimatedTile(doorBottomClosed,doorBottomOpen, false, true, false, true, true,false,false);
					break;
				case EObjectType::ExitTopLeft:
					//Set 'at' as an exit door top left tile
					at = gcnew AnimatedTile(exitTopLeftClosed,exitTopLeftOpen, false, false, false, false, false,false,true);
					break;
				case EObjectType::ExitTopRight:
					//Set 'at' as an exit door top right tile
					at = gcnew AnimatedTile(exitTopRightClosed,exitTopRightOpen,false, false, false, false, false,false,true);
					break;
				case EObjectType::ExitMidLeft:
					//Set 'at' as an exit door middle left tile
					at = gcnew AnimatedTile(exitMidLeftClosed,exitMidLeftOpen, false, false, false, false, false,false,true);
					break;
				case EObjectType::ExitMidRight:
					//Set 'at' as an exit door middle right tile
					at = gcnew AnimatedTile(exitMidRightClosed,exitMidRightOpen, false, false, false, false, false,false,true);
					break;
				case EObjectType::ExitBottomLeft:
					//Set 'at' as an exit door bottom left tile
					at = gcnew AnimatedTile(exitBottomLeftClosed,exitBottomLeftOpen, false, false, false, false, false,false,true);
					break;
				case EObjectType::ExitBottomRight:
					//Set 'at' as an exit door bottom right tile
					at = gcnew AnimatedTile(exitBottomRightClosed,exitBottomRightOpen, false, false, false, false, false,false,true);
					break;
				default:
					//The value was weird so create an air tile
					at = gcnew AnimatedTile(air, air, false, false, false, false, false,false,false);
					break;
				}//End switch
				//Set the transparency colour to white
				at->SetTileTransparency(Color::White);

				//Set the tile into the map
				tiles[row, col] = at;
			}//End column for
		}//End row for

		//Close the stream to the file
		reader->Close();
	}//End of try
	catch (Exception^ e)
	{
		//There was an error creating the map, show a message box
		MessageBox::Show("Unfortunately there was an error loading the object map\n" + e->Message);
	}//End catch
}//End LoadFromFile(filepath)

void ObjectMap::LoadTriggersFromFile(String^ filepath)
{
	//Attempts to load the trigger configuration from the levels triggers.csv
	try
	{
		//Open up a stream to the trigger file
		StreamReader^ reader = gcnew StreamReader(filepath + "/triggers.csv");

		//Loop until the end of the stream
		while(!reader->EndOfStream)
		{
			//Read a line from the file
			String^ line = reader->ReadLine();
			//Split that line into its comma delimited parts
			array<String^>^ parts = line->Split(',');

			//Get the target location
			array<String^>^ targetCoords = parts[0]->Split(':');
			int targetY = Convert::ToInt32(targetCoords[0]);
			int targetX = Convert::ToInt32(targetCoords[1]);

			//Loop through the rest of the values in the parts array
			for (int i = 1; i < parts->Length; i++)
			{
				//Get the coordinates of the trigger tile
				array<String^>^ triggerCoords = parts[i]->Split(':');
				int triggerY = Convert::ToInt32(triggerCoords[0]);
				int triggerX = Convert::ToInt32(triggerCoords[1]);
				
				SetTileTrigger(targetX,targetY,triggerX,triggerY);
			}//End for
		}//End while
	}//End try
	catch (Exception^ e)
	{
		//There was an error while loading the triggers
		MessageBox::Show("Error loading level triggers\n" + e->Message);
	}//End catch
}//End LoadTriggersFromFile(filepath)

void ObjectMap::SetTileTrigger(int xIndex, int yIndex, int triggerX, int triggerY)
{
	//Manually set a tile trigger using the given indices
	try
	{
		
		AnimatedTile^ target = nullptr;
		
		//Check if the targets coordinate values are in bounds
		if((yIndex >= 0 && yIndex < GetRows()) && (xIndex >= 0 && xIndex < GetColumns()))
		{
			target = tiles[yIndex, xIndex];
		}
		else //The values are out of bounds
		{
			throw gcnew Exception("Trigger target outside of bounds of map");
		}

		//If the target is a valid tile
		if (target != nullptr)
		{
			AnimatedTile^ trigger = nullptr;
			//Check if the triggers coordinate values are in bounds
			if((triggerY >= 0 && triggerY < GetRows()) && (triggerX >= 0 && triggerX < GetColumns()))
			{
				trigger = tiles[triggerY,triggerX];
			}
			else //The values are out of bounds
			{
				throw gcnew Exception("The triggers coordinates were outside of bounds of map");
			}

			//If the trigger is a valid tile
			if (trigger != nullptr)
			{
				//Add the trigger to the targets trigger list
				target->AddTrigger(trigger);
				System::Diagnostics::Debug::WriteLine("Adding trigger ["+triggerY+","+triggerX+"] to target ["+yIndex+","+xIndex+"]");
			}
			else //The trigger was not a valid tile
			{
				throw gcnew Exception("trigger was null");
			}//End null trigger else
		}//End valid target if
		else //The target was invalid
		{
			throw gcnew Exception("target was null");
		}//End null target else
	}//End try
	catch (Exception^ e)
	{
		//An error occured adding the trigger
		MessageBox::Show("Error assigning manual trigger: " + e->Message);
	}//End catch
}//End SetTileTrigger(xIndex,yIndex,triggerX,triggerY)

void ObjectMap::TriggerTileAtPoint(Point p)
{
	//Tells the tile at the given point to trigger itself

	//Get the map index of the given point
	int tileX = p.X/tileSize;
	int tileY = p.Y/tileSize;

	//Check if tileX and tileY are in bounds
	if(tileY >= 0 && tileY < GetRows())
	{
		System::Diagnostics::Debug::WriteLine("Attempted to trigger tile that was out of bounds");
	}
	else//The indices are within the boinds of the map
	{
		//check if the tile is a clickable tile
		if(tiles[tileY, tileX]->GetCanBeClicked())
		{
			//Trigger the tile
			tiles[tileY, tileX]->Trigger();
		}//End CanBeClicked if
	}//End outofbounds else
}//End TriggerTileAtPoint(p)

void ObjectMap::UpdateAll()
{
	//Tells all the tiles to update themselves

	//Loop through every row
	for (int r = 0; r < GetRows(); r++)
	{
		//Loop through every column
		for (int c = 0; c < GetColumns(); c++)
		{
			//Tell the tile at c,r to update its state
			tiles[r,c]->UpdateState();
		}//End column for
	}//End row for
}//End UpdateALl()