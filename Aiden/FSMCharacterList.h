#pragma once
#include "FiniteStateCharacter.h"
#include "ProjectileList.h"

/*
	Author: Kierran McPherson
	Date: 18/11/2013
	Purpose: This class defines a LinkedList of FiniteStateCharacters. It allows for mass control and manipulation
			 of all of the FiniteStateCharacters in the List
	Known Bugs:
*/
ref class FSMCharacterList
{
private:
	FiniteStateCharacter^ head;
	FiniteStateCharacter^ tail;
public:
	FSMCharacterList();
	FSMCharacterList(FiniteStateCharacter^ first);

	int Count();
	int CountDead();
	void Add(FiniteStateCharacter^ node);
	void Remove(FiniteStateCharacter^ node);
	void DrawAll(Point viewportWorldPosition, Size viewportSize);
	bool InViewport(Point characterLocation, Size characterSize, Size viewportSize);
	void UpdateStates(AidenCharacter^ player, BitArray^ keys);
	void PerformActions(AidenCharacter^ player, BitArray^ keys, ProjectileList^ bullets);
	FiniteStateCharacter^ GetClickedNPC(Point clickWorldLocation);
};

