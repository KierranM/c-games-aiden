#pragma once
	using namespace System;
	using namespace System::ComponentModel;
	using namespace System::Collections;
	using namespace System::Windows::Forms;
	using namespace System::Data;
	using namespace System::Drawing;

/*
	Author: Kierran McPherson
	Date: 18/11/2013
	Purpose: This class defines a normal Tile that has an image, and can be lethal, solid, and special barriers
	Known Bugs:
*/
ref class Tile
{
protected:
	Bitmap^ tileImage;
	bool isLethal;
	bool isSolid;
	bool isAidenBarrier;
public:
	Tile();
	Tile(String^ filename, bool startLethal, bool startIsSolid, bool startAidenBarrier);
	
	//Gets and Sets
	virtual Bitmap^ GetTileImage()	{return tileImage;}
	void SetTileImage(Bitmap^ image)	{tileImage = image;}

	virtual void SetTileTransparency(Color transparentColor)	{tileImage->MakeTransparent(transparentColor);}

	bool GetIsLethal()	{return isLethal;}
	void SetIsLethal(bool lethal)	{isLethal = lethal;}

	virtual bool GetIsSolid()	{return isSolid;}
	virtual bool GetIsAidenBarrier()	{return isAidenBarrier;}
};

