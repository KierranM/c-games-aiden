#pragma once
#include "FiniteStateCharacter.h"
#include "TriggerList.h"
#include "ProjectileList.h"

#define AUTO_TURRET_VIEW_DISTANCE 500
#define AIMING_INACCURACY 20
#define AIMING_SPEED 2

//Inside namespace to prevent scope issues
namespace AutoTurretStates{
	public enum ECharacterState
	{
		Monitoring,
		Aiming,
		Firing,
		Disabled
	};
};

/*
	Author: Kierran McPherson
	Date: 22/11/2013
	Purpose: This class defines the auto turret. A special FSM controlled character that cannot move, but can shoot in non linear directions
	Known Bugs:
*/

ref class AutoTurret :
public FiniteStateCharacter
{
private:
	TriggerList^ triggers;
	ProjectileList^ bullets;
	bool doneAiming;
	Point aimingLocation;
public:
	AutoTurret(
		array<String^, TWO>^ filenames, 
		array<int,TWO>^ frameNumbers, 
		array<Size>^ frameSize,
		Point startLocation,
		int startAction,
		int startXDeadZone,
		int startYDeadZone,
		Graphics^ startCanvas,
		Random^ startRandom,
		Rectangle startBounds,
		TileMap^ startMap,
		ObjectMap^ startObjects,
		ProjectileList^ startBullets
	);

	virtual void UpdateState(AidenCharacter^ player, BitArray^ keys) override;
	virtual void PerformActions(AidenCharacter^ player, BitArray^ keys) override;
	void Fire();
	bool InAimingZone(Character^ character);
};

