#pragma once
#include "FiniteStateCharacter.h"
#include "ProjectileList.h"
#define PAUSE_CHANCE 40
#define GUARD_JUMP_STRENGTH -18
#define GUARD_MOVE_SPEED 8
#define GUARD_VIEW_DISTANCE 500
#define SHOT_ORIGIN_Y 21
#define SHOT_ORIGIN_X 4
#define POSSESSED_SHOT_ORIGIN_Y 42
#define POSSESSED_SHOT_ORIGIN_X 8

#define BULLET_MIN_SPEED 40
#define BULLET_MAX_SPEED 60

//Inside a namespace to prevent scope issues
namespace GuardStates
{
	private enum ECharacterState
	{
		Patrolling,
		Paused,
		Pursuing,
		Engaging,
		Possessed,
		Falling,
		Dead
	};
};

/*
	Author: Kierran McPherson
	Date: 18/11/2013
	Purpose: This class defines a Guard NPC which is a Character in the game that is controlled by an FSM,
			 it has 7 different states and can shoot bullets
	Known Bugs:
*/

ref class Guard :
public FiniteStateCharacter
{
private:
	ProjectileList^ bullets;
public:
	Guard(
		array<String^, TWO>^ filenames, 
		array<int,TWO>^ frameNumbers, 
		array<Size>^ frameSize,
		Point startLocation,
		int startAction,
		int startXDeadZone,
		int startYDeadZone,
		Graphics^ startCanvas,
		Random^ startRandom,
		Rectangle startBounds,
		TileMap^ startMap,
		ObjectMap^ startObjects,
		int startPatrollingTime,
		int startPatrollingTicks,
		int startPausedTime,
		int startPausedTicks,
		bool startCanBePossessed,
		ProjectileList^ startBullets
	);

	virtual void UpdateState(AidenCharacter^ player, BitArray^ keys) override;
	virtual void PerformActions(AidenCharacter^ player, BitArray^ keys) override;

	virtual void PossessedActions(BitArray^ keys) override;

	void Pursue(AidenCharacter^ player);
	void Engage();
};

