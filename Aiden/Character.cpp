#include "stdafx.h"
#include "Character.h"

Character::Character(){

}

Character::Character(
		array<String^, TWO>^ filenames, 
		array<int, TWO>^ frameNumbers, 
		array<Size>^ frameSize, 
		Point startLocation,
		int startAction,
		int numDirections,
		int startXDeadZone,
		int startYDeadZone,
		Graphics^ startCanvas,
		Random^ startRandom,
		Rectangle startBounds,
		TileMap^ startMap,
		ObjectMap^ startObjects
	)
{
	//Sets up the sprite using the given parameters
	random = startRandom;
	canvas = startCanvas;
	boundary = startBounds;
	velocity = Point(0,0);
	isAlive =  true;
	map = startMap;
	objects = startObjects;
	currentAction = startAction;

	//Store the sprites deadzones
	xDeadzone = startXDeadZone;
	yDeadzone = startYDeadZone;

	location = startLocation;

	//Set up the hitbox
	
	int hitBoxX = location.X + xDeadzone;
	int hitboxY = location.Y + yDeadzone;
	int hitboxWidth = frameSize[0].Width - (xDeadzone * TWO);
	int hitboxHeight = frameSize[0].Height - (yDeadzone * TWO);
	hitbox = Rectangle(hitBoxX,hitboxY, hitboxWidth, hitboxHeight);

	//Load the spritesheets

	//Get the number of actions that this sprite uses
	int actions = filenames->GetLength(0);

	//Create spritesheets array with the same length as filenames
	spriteSheets = gcnew array<SpriteSheet^,TWO>(actions, numDirections);

	//Loop for the number of actions
	for (int action = 0; action < actions; action++)
	{
		//Loop for the number of directions
		for (int direction = 0; direction < numDirections; direction++)
		{
			//Add the spritesheet
			spriteSheets[action,direction] = gcnew SpriteSheet(filenames[action,direction], frameNumbers[action,direction], frameSize[action].Width, frameSize[action].Height);
		}//End direction for
	}//End action for
}//End constructor 13 params

void Character::Draw()
{
	//Simple draw
	canvas->DrawImage(spriteSheets[currentAction,0]->GetFrame(), hitbox);
}//End Draw()

void Character::Draw(Point viewportWorldLocation)
{
	//Draws the Character to the appropriate location relative to the viewports position
	//Get the tile at the center of the sprite
	Point center = GetCenter();
	int centerTileX = center.X/map->GetTileSize();
	int centerTileY = center.Y/map->GetTileSize();

	
	//get the characters viewport location
	Point characterViewportLocation = CalculateViewPortLocation(viewportWorldLocation);

	//Default behaviour for getting the direction
	eDirection direction;
	if (velocity.X < 0)
	{
		direction = eDirection::Left;
	}
	else
	{
		direction = eDirection::Right;
	}

	//Draws the sprite at the given location
	canvas->DrawImage(spriteSheets[currentAction, direction]->GetFrame(), characterViewportLocation.X, characterViewportLocation.Y);

	//Display the sprites hitbox when in debug mode
	#ifdef _DEBUG
		canvas->DrawRectangle(Pens::Black, characterViewportLocation.X + xDeadzone, characterViewportLocation.Y + yDeadzone, hitbox.Width, hitbox.Height);
	#endif
}//End Draw(viewportWorldLocation)

void Character::Update()
{
	//get the current direction
	eDirection direction;
	if (velocity.X < 0)
	{
		direction = eDirection::Left;
	}
	else
	{
		direction = eDirection::Right;
	}
	//Updates the character
	spriteSheets[currentAction,direction]->UpdateCurrentFrame();
}//End Update()

Point Character::GetCenter()
{
	//Gets the mid point of the character
	Size currentFrameSize = spriteSheets[currentAction,0]->GetFrameSize();
	int x = location.X + currentFrameSize.Width/TWO;
	int y = location.Y + currentFrameSize.Height/TWO;

	return Point(x,y);
}//End GetCenter()

Point Character::CalculateViewPortLocation(Point viewportWorldPosition)
{
	//Calculates the characters location relevant to the given viewport location
	Point characterViewportPosition;

	//The sprites location on the viewport is the characters world location - the viewports world location
	characterViewportPosition.X = location.X - viewportWorldPosition.X;
	characterViewportPosition.Y = location.Y - viewportWorldPosition.Y;

	return characterViewportPosition;
}//End CalculateViewPortLocation(viewportWorldPositon)

void Character::Move()
{
	//Create a copy of the hitbox
	Rectangle onMove = hitbox;

	//Move the hitbox
	onMove.X += velocity.X;
	onMove.Y += velocity.Y;
	
	//Check if the character is hitting a solid tile on its left or right sides
	if (!CheckTileX(onMove))
	{
		//Check if the character has ran off the right or left edge of the world
		if (!CheckHitEdgeX(onMove))
		{
			//It is a valid move so do the move
			hitbox.X = onMove.X;
			location.X += velocity.X;
		}//End if CheckEdgeX
	}//End if CheckTileX

	//Check if the character is hitting a solid tile on its top or bottom sides
	if(!CheckTileY(onMove))
	{
		//Check if the character has ran off the bottom or top of the world
		if (!CheckHitEdgeY(onMove))
		{
			//It is a valid move so do the move
			hitbox.Y = onMove.Y;
			location.Y += velocity.Y;
		}//End if CheckEdgeY
	}//End if CheckTileY
}//End Move()

bool Character::CheckTileX(Rectangle toCheck)
{
	//Check if one of the tiles on its vertical sides (left or right) is solid

	//Get the maps tile size
	int tileSize = map->GetTileSize();
	array<Point>^ sidePoints = gcnew array<Point>(toCheck.Height);

	//Get all the points along the left edge
	for (int i = 0; i < toCheck.Height; i++)
	{
		sidePoints[i] = Point(toCheck.X, toCheck.Y + i);
	}

	bool solidTile = false;
	
	//loop through all of the points along the edges
	for (int i = TWO; i < toCheck.Height - TWO; i++)
	{
		//Calculate the tile Y index at the Y value for the current Point
		int tileY = sidePoints[i].Y/tileSize;
		
		//Calculate the Tile x index for both the left and right X values of the current point
		int tileX = sidePoints[i].X/tileSize;
		int rightTileX = (sidePoints[i].X + toCheck.Width)/tileSize;

		//Check if the tile in the map or objects map is solid
		if((map->GetIsSolid(tileX,tileY) || map->GetIsSolid(rightTileX,tileY)) || (objects->GetIsSolid(tileX, tileY) || objects->GetIsSolid(rightTileX, tileY)))
		{
			solidTile = true;
		}

		//Check if the tile is lethal
		if ((map->GetIsLethal(tileX,tileY) || map->GetIsLethal(rightTileX,tileY)) || (objects->GetIsLethal(tileX, tileY) || objects->GetIsLethal(rightTileX, tileY)))
		{
			//Kill the character
			isAlive = false;
		}//End tile lethal if
	}//End for

	return solidTile;
}//End CheckTileX(toCheck)

bool Character::CheckTileY(Rectangle toCheck)
{
	//Checks if any of the tiles on the horizontal edges (top and bottom) are solid

	//Get the maps tile size
	int tileSize = map->GetTileSize();
	array<Point>^ sidePoints = gcnew array<Point>(toCheck.Width);

	//Get every point along the top edge
	for (int i = 0; i < toCheck.Width; i++)
	{
		sidePoints[i] = Point(toCheck.X + i, toCheck.Y);
	}

	bool solidTile = false;
	
	//Loop through all the points checking if any of the tiles are solid
	for (int i = TWO; i < toCheck.Width - TWO; i++)
	{
		//Get the X index of the tile for the current Points x value
		int tileX = sidePoints[i].X/tileSize;

		//Get the Y index of the tile for both the top and bottom edge from the current Points y value
		int tileY = sidePoints[i].Y/tileSize;
		int bottomTileY = (sidePoints[i].Y + toCheck.Height)/tileSize;

		//Check if the tile in the map or objects map is solid
		if(map->GetIsSolid(tileX,tileY) || map->GetIsSolid(tileX,bottomTileY) || (objects->GetIsSolid(tileX, tileY) || objects->GetIsSolid(tileX, bottomTileY)))
		{
			solidTile = true;
		}

		//Check if the tile is lethal
		if ((map->GetIsLethal(tileX,tileY) || map->GetIsLethal(tileX,bottomTileY)) || (objects->GetIsLethal(tileX, tileY) || objects->GetIsLethal(tileX, bottomTileY)))
		{
			//Kill the character
			isAlive = false;
		}//End lethal tile if
	}//End for

	return solidTile;
}//End CheckTileY(toCheck)

bool Character::CheckHitEdgeX(Rectangle toCheck)
{
	//Check if the character has hit the left or right edges of the boundary
	bool hitEdge = false;

	if (toCheck.X <= boundary.X || toCheck.Right >= boundary.Right)
	{
		hitEdge = true;
	}

	return hitEdge;
}//CheckHitEdgeX(toCheck)

bool Character::CheckHitEdgeY(Rectangle toCheck)
{
	//Check if the character has hit the top or bottom edges of the boundary
	bool hitEdge = false;

	if (toCheck.Y <= boundary.Y || toCheck.Bottom >= boundary.Bottom)
	{
		hitEdge = true;
	}

	return hitEdge;
}//CheckHitEdgeY(toCheck)