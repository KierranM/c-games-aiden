#include "stdafx.h"
#include "ProjectileList.h"

ProjectileList::ProjectileList(void)
{
	//Make both pointers point to nothing
	head = nullptr;
	tail = nullptr;
}//End constructor no params


ProjectileList::ProjectileList(Projectile^ first){
	//Set both pointers to point to the first pellet
	head = first;
	tail = first;
}//End constructor 1 params

ProjectileList::ProjectileList(TileMap^ startMap, ObjectMap^ startObjects, String^ startImageFilepath, Graphics^ startCanvas, Rectangle startWorld)
{
	//Create the projectile list with the given values
	map = startMap;
	objects = startObjects;
	imageFilepath = startImageFilepath;
	world = startWorld;
	canvas = startCanvas;

	//Set head and tail to nullptr
	head = nullptr;
	tail = nullptr;
}//End constructor 5 params


int ProjectileList::Count(){
	//Loops through the list and counts the number of projectiles
	int count = 0;
	Projectile^ nodeWalker = head;
	while(nodeWalker != nullptr){
		nodeWalker = nodeWalker->Next;
		count++;
	}

	return count;
}//End Count()


void ProjectileList::Add(Projectile ^node){
	//Adds the given projectile to the list
	if(tail == nullptr){ //Empty list
		head = node;
		tail = node;
	}
	else{ //Adding to end
		tail->Next = node;
		tail = node;
	}
}//End Add(node)

void ProjectileList::Add(Point location, Point velocity, eDirection direction)
{
	//Add a new projectile to the list based on the given location, velocity, and direction

	//Create the node
	Projectile^ newNode = gcnew Projectile(location,velocity,direction,world,map,objects,canvas,imageFilepath);
	//Add the node to the list
	Add(newNode);
}//End Add(location,velocity,direction)

void ProjectileList::Remove(Projectile ^node){
	//Removes the given node from the list

	Projectile^ nodeWalker;

	//start at the beginning of the list
	nodeWalker = head;

	//If the node to be deleted is the head
	if(node == head){
		//If the node is the head and has no next
		if(node->Next == nullptr){
			head = nullptr;
			tail = nullptr;
		}
		else{ //Node is the first but not the only item in the list
			head = head->Next;
		}
	}//End of is head if
	else{ 
		//The node to be deleted is not head
		Projectile^ temp;
		while(nodeWalker->Next != node){
			nodeWalker = nodeWalker->Next;
		}
		//Store the node
		temp = nodeWalker->Next;
		//Check if the node that is being removed is the tail
		if(temp == tail){
			//Make the tail the current node and set its next to nothing
			tail = nodeWalker;
			nodeWalker->Next = nullptr;
		}
		else{
			//Otherwise make the list skip over the node that is being removed
			nodeWalker->Next = temp->Next;
		}

	}//End of not head else
}//End of Remove(node)


void ProjectileList::DrawAll(Point viewportWorldPosition, Size viewportSize){
	//Loops through all projectiles in the list and tells them to draw themselves if they are within the viewport area
	Projectile^ nodeWalker = head;

	//The list is not empty
	if(nodeWalker != nullptr){

		//loop until there are no more nodes
		while(nodeWalker != nullptr){

			//Get the projectiles location relevant to the viewport
			Point location = nodeWalker->GetLocation();
			int relativeX = location.X - viewportWorldPosition.X;
			int relativeY = location.Y - viewportWorldPosition.Y;
			Point projectileViewportPosition = Point(relativeX,relativeY);

			//Get the size of the projectiles current frame
			Size projectileSize = nodeWalker->GetSize();

			//If the projectile is within the viewport
			if (InViewport(projectileViewportPosition, projectileSize, viewportSize))
			{
				//The projectile is in the viewport so draw it
				nodeWalker->Draw(viewportWorldPosition);
			}
			//Move on to the next node
			nodeWalker = nodeWalker->Next;
		}//End of while
	}//End of if
}//End of DrawAll(viewportWorldPosition, viewportSize)

bool ProjectileList::InViewport(Point projectileLocation, Size projectileSize, Size viewportSize)
{
	//Checks if the given coordinates are within the viewports area

	//Given coordinates are higher than the left side of the viewport
	if ((projectileLocation.X + projectileSize.Width) >= 0)
	{
		//Given coordinates are less than the right side of the viewport
		if (projectileLocation.X <= (viewportSize.Width))
		{
			//Given coordinates are higher than the top side of the viewport
			if ((projectileLocation.Y + projectileSize.Height) >= 0)
			{
				//Given coordinates are less than the bottom side of the viewport
				if (projectileLocation.Y <= (viewportSize.Height))
				{
					//The given coordinates are in the viewports area
					return true;
				}//End bottom if
			}//end top if
		}//end right if
	}//end left if

	//The given coordinates were not in the viewports area
	return false;
}//End InViewport(projectileLocation, projectileSize, viewportSize)

void ProjectileList::UpdateAll()
{
	//Loops through all projectiles in the list and tells them to update themselves
	Projectile^ nodeWalker = head;

	//if the list is not empty
	if(nodeWalker != nullptr){
		//loop until there are no more nodes
		while(nodeWalker != nullptr){
			nodeWalker->Move();
			Projectile^ temp = nodeWalker;
			//Check if the node is dead
			if (nodeWalker->GetIsAlive() == false)
			{
				Remove(nodeWalker);
			}
			//Move on to the next node
			nodeWalker = temp->Next;
		}//End while
	}//End empty list if
}//End UpdateAll()

void ProjectileList::CollisionsWith(Character^ character)
{
	//Loops through the list and checks if any of the projectiles collide with the character
	Projectile^ nodeWalker = head;

	//If the list is not emtpy
	if(nodeWalker != nullptr){
		//Loop until there are no more nodes
		while(nodeWalker != nullptr){
			bool collision = nodeWalker->CollidesWith(character);
			if (collision)
			{
				//Kill the character
				character->SetIsAlive(false);
				//Kill the projectile
				nodeWalker->SetIsAlive(false);
			}
			//move on to the next node
			nodeWalker = nodeWalker->Next;
		}//End while
	}//End empty list if
}//End CollisionsWith(character)